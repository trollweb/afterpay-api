# Trollweb\AfterPayApi\CommonApi

All URIs are relative to *https://api.afterpay.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**commonCustomerLookup**](CommonApi.md#commonCustomerLookup) | **POST** /api/v3/lookup/customer | Returns the customers address based on social security number or mobile number.
[**commonGetStatus**](CommonApi.md#commonGetStatus) | **GET** /api/v3/status | Gets the status of the service
[**commonGetVersion**](CommonApi.md#commonGetVersion) | **GET** /api/v3/version | Gets the version of the service
[**commonValidateAddress**](CommonApi.md#commonValidateAddress) | **POST** /api/v3/validate/address | Check of the delivered customer addresses as well as a phonetic and associative identification of duplicates.  Additionally, checks of client specific negative or positive lists can be processed. Usually, the AddressCheck is  used for the pure verification of the address data e.g. for registration processes.
[**commonValidateBankAccount**](CommonApi.md#commonValidateBankAccount) | **POST** /api/v3/validate/bank-account | Validates and evaluates the account and bank details in the context of direct debit payment.  It is possible to transfer either the combination of BankCode and AccountNumber or IBAN and BIC


# **commonCustomerLookup**
> \Trollweb\AfterPayApi\Model\CustomerLookupResponse commonCustomerLookup($request)

Returns the customers address based on social security number or mobile number.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CommonApi();
$request = new \Trollweb\AfterPayApi\Model\CustomerLookupRequest(); // \Trollweb\AfterPayApi\Model\CustomerLookupRequest | 

try {
    $result = $api_instance->commonCustomerLookup($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonCustomerLookup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\CustomerLookupRequest**](../Model/CustomerLookupRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\CustomerLookupResponse**](../Model/CustomerLookupResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonGetStatus**
> \Trollweb\AfterPayApi\Model\GetStatusResponse commonGetStatus()

Gets the status of the service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CommonApi();

try {
    $result = $api_instance->commonGetStatus();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonGetStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\AfterPayApi\Model\GetStatusResponse**](../Model/GetStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonGetVersion**
> \Trollweb\AfterPayApi\Model\GetVersionResponse commonGetVersion()

Gets the version of the service

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CommonApi();

try {
    $result = $api_instance->commonGetVersion();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonGetVersion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\AfterPayApi\Model\GetVersionResponse**](../Model/GetVersionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonValidateAddress**
> \Trollweb\AfterPayApi\Model\ValidateAddressResponse commonValidateAddress($request)

Check of the delivered customer addresses as well as a phonetic and associative identification of duplicates.  Additionally, checks of client specific negative or positive lists can be processed. Usually, the AddressCheck is  used for the pure verification of the address data e.g. for registration processes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CommonApi();
$request = new \Trollweb\AfterPayApi\Model\ValidateAddressRequest(); // \Trollweb\AfterPayApi\Model\ValidateAddressRequest | 

try {
    $result = $api_instance->commonValidateAddress($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonValidateAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\ValidateAddressRequest**](../Model/ValidateAddressRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\ValidateAddressResponse**](../Model/ValidateAddressResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **commonValidateBankAccount**
> \Trollweb\AfterPayApi\Model\ValidateBankAccountResponse commonValidateBankAccount($request)

Validates and evaluates the account and bank details in the context of direct debit payment.  It is possible to transfer either the combination of BankCode and AccountNumber or IBAN and BIC

API call will validate given bank account is passing account format rules.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CommonApi();
$request = new \Trollweb\AfterPayApi\Model\ValidateBankAccountRequest(); // \Trollweb\AfterPayApi\Model\ValidateBankAccountRequest | 

try {
    $result = $api_instance->commonValidateBankAccount($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->commonValidateBankAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\ValidateBankAccountRequest**](../Model/ValidateBankAccountRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\ValidateBankAccountResponse**](../Model/ValidateBankAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

