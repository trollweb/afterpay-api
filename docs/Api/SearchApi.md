# Trollweb\AfterPayApi\SearchApi

All URIs are relative to *https://api.afterpay.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchSearchInvoicesProcessed**](SearchApi.md#searchSearchInvoicesProcessed) | **POST** /api/v3/search/invoices/processed | Returns invoices matching criteria


# **searchSearchInvoicesProcessed**
> \Trollweb\AfterPayApi\Model\MarkInvoicesProcessedResponse searchSearchInvoicesProcessed($request)

Returns invoices matching criteria

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\SearchApi();
$request = new \Trollweb\AfterPayApi\Model\MarkInvoicesProcessedRequest(); // \Trollweb\AfterPayApi\Model\MarkInvoicesProcessedRequest | 

try {
    $result = $api_instance->searchSearchInvoicesProcessed($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchSearchInvoicesProcessed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\MarkInvoicesProcessedRequest**](../Model/MarkInvoicesProcessedRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\MarkInvoicesProcessedResponse**](../Model/MarkInvoicesProcessedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

