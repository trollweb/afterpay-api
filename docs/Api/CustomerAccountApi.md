# Trollweb\AfterPayApi\CustomerAccountApi

All URIs are relative to *https://api.afterpay.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerAccountAddEvent**](CustomerAccountApi.md#customerAccountAddEvent) | **POST** /api/v3/customer-account/{accountNumber}/event | Add event to account
[**customerAccountAddTransaction**](CustomerAccountApi.md#customerAccountAddTransaction) | **POST** /api/v3/customer-account/{accountNumber}/transaction | Add transaction to account
[**customerAccountApplyForCredit**](CustomerAccountApi.md#customerAccountApplyForCredit) | **POST** /api/v3/customer-account | Apply for customer account credit
[**customerAccountCreateUser**](CustomerAccountApi.md#customerAccountCreateUser) | **POST** /api/v3/customer-account/{accountNumber}/user | Add user to account
[**customerAccountCreditLimit**](CustomerAccountApi.md#customerAccountCreditLimit) | **GET** /api/v3/customer-account/{accountNumber}/credit-limit | Get remaining account credit limit
[**customerAccountDeleteTransaction**](CustomerAccountApi.md#customerAccountDeleteTransaction) | **DELETE** /api/v3/customer-account/{accountNumber}/transaction/{transactionId} | Cancels transaction made by account
[**customerAccountLookupBySSN**](CustomerAccountApi.md#customerAccountLookupBySSN) | **GET** /api/v3/customer-account/{ssn} | Find customer account by SSN (social security number)


# **customerAccountAddEvent**
> \Trollweb\AfterPayApi\Model\AddEventToAccountResponse customerAccountAddEvent($account_number, $request)

Add event to account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$account_number = "account_number_example"; // string | Account number
$request = new \Trollweb\AfterPayApi\Model\AddEventToAccountRequest(); // \Trollweb\AfterPayApi\Model\AddEventToAccountRequest | 

try {
    $result = $api_instance->customerAccountAddEvent($account_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountAddEvent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **string**| Account number |
 **request** | [**\Trollweb\AfterPayApi\Model\AddEventToAccountRequest**](../Model/AddEventToAccountRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\AddEventToAccountResponse**](../Model/AddEventToAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountAddTransaction**
> \Trollweb\AfterPayApi\Model\AddTransactionResponse customerAccountAddTransaction($account_number, $request)

Add transaction to account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$account_number = "account_number_example"; // string | Account number
$request = new \Trollweb\AfterPayApi\Model\AddTransactionRequest(); // \Trollweb\AfterPayApi\Model\AddTransactionRequest | Request

try {
    $result = $api_instance->customerAccountAddTransaction($account_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountAddTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **string**| Account number |
 **request** | [**\Trollweb\AfterPayApi\Model\AddTransactionRequest**](../Model/AddTransactionRequest.md)| Request |

### Return type

[**\Trollweb\AfterPayApi\Model\AddTransactionResponse**](../Model/AddTransactionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountApplyForCredit**
> \Trollweb\AfterPayApi\Model\ApplyForAccountCreditResponse customerAccountApplyForCredit($request)

Apply for customer account credit

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$request = new \Trollweb\AfterPayApi\Model\ApplyForAccountCreditRequest(); // \Trollweb\AfterPayApi\Model\ApplyForAccountCreditRequest | 

try {
    $result = $api_instance->customerAccountApplyForCredit($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountApplyForCredit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\ApplyForAccountCreditRequest**](../Model/ApplyForAccountCreditRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\ApplyForAccountCreditResponse**](../Model/ApplyForAccountCreditResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountCreateUser**
> \Trollweb\AfterPayApi\Model\CreateUserResponse customerAccountCreateUser($account_number, $request)

Add user to account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$account_number = "account_number_example"; // string | Account number
$request = new \Trollweb\AfterPayApi\Model\CreateUserRequest(); // \Trollweb\AfterPayApi\Model\CreateUserRequest | 

try {
    $result = $api_instance->customerAccountCreateUser($account_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountCreateUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **string**| Account number |
 **request** | [**\Trollweb\AfterPayApi\Model\CreateUserRequest**](../Model/CreateUserRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\CreateUserResponse**](../Model/CreateUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountCreditLimit**
> \Trollweb\AfterPayApi\Model\GetRemainingLimitResponse customerAccountCreditLimit($account_number)

Get remaining account credit limit

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$account_number = "account_number_example"; // string | Account number

try {
    $result = $api_instance->customerAccountCreditLimit($account_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountCreditLimit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **string**| Account number |

### Return type

[**\Trollweb\AfterPayApi\Model\GetRemainingLimitResponse**](../Model/GetRemainingLimitResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountDeleteTransaction**
> \Trollweb\AfterPayApi\Model\DeleteTransactionResponse customerAccountDeleteTransaction($account_number, $transaction_id)

Cancels transaction made by account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$account_number = "account_number_example"; // string | Account number
$transaction_id = "transaction_id_example"; // string | Transaction ID

try {
    $result = $api_instance->customerAccountDeleteTransaction($account_number, $transaction_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountDeleteTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **string**| Account number |
 **transaction_id** | **string**| Transaction ID |

### Return type

[**\Trollweb\AfterPayApi\Model\DeleteTransactionResponse**](../Model/DeleteTransactionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAccountLookupBySSN**
> \Trollweb\AfterPayApi\Model\LookupAccountBySsnResponse customerAccountLookupBySSN($ssn)

Find customer account by SSN (social security number)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CustomerAccountApi();
$ssn = "ssn_example"; // string | 

try {
    $result = $api_instance->customerAccountLookupBySSN($ssn);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerAccountApi->customerAccountLookupBySSN: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssn** | **string**|  |

### Return type

[**\Trollweb\AfterPayApi\Model\LookupAccountBySsnResponse**](../Model/LookupAccountBySsnResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

