# Trollweb\AfterPayApi\ProductApi

All URIs are relative to *https://api.afterpay.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productAvailableInstallmentPlans**](ProductApi.md#productAvailableInstallmentPlans) | **POST** /api/v3/lookup/installment-plans | Returns the available installment plans for the specific product/basket value. Returns monthly installment amount, interest and fees. Typically used on a product page.


# **productAvailableInstallmentPlans**
> \Trollweb\AfterPayApi\Model\AvailableInstallmentPlansResponse productAvailableInstallmentPlans($request)

Returns the available installment plans for the specific product/basket value. Returns monthly installment amount, interest and fees. Typically used on a product page.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\ProductApi();
$request = new \Trollweb\AfterPayApi\Model\AvailableInstallmentPlansRequest(); // \Trollweb\AfterPayApi\Model\AvailableInstallmentPlansRequest | 

try {
    $result = $api_instance->productAvailableInstallmentPlans($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAvailableInstallmentPlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\AvailableInstallmentPlansRequest**](../Model/AvailableInstallmentPlansRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\AvailableInstallmentPlansResponse**](../Model/AvailableInstallmentPlansResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

