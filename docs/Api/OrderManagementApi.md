# Trollweb\AfterPayApi\OrderManagementApi

All URIs are relative to *https://api.afterpay.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orderManagementAddShippingDetails**](OrderManagementApi.md#orderManagementAddShippingDetails) | **POST** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details | Add new shipping details information to the capture.
[**orderManagementCapture**](OrderManagementApi.md#orderManagementCapture) | **POST** /api/v3/orders/{orderNumber}/captures | Completes the payment that has been authorized. Typically done when the order is shipped. Can be a full or partial capture of the order amount.
[**orderManagementDeleteShippingDetails**](OrderManagementApi.md#orderManagementDeleteShippingDetails) | **DELETE** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details/{shippingNumber} | Delete shipping details information from the specific capture.
[**orderManagementGetCapture**](OrderManagementApi.md#orderManagementGetCapture) | **GET** /api/v3/orders/{orderNumber}/captures/{captureNumber} | Returns all or specific captured payments of the order
[**orderManagementGetOrder**](OrderManagementApi.md#orderManagementGetOrder) | **GET** /api/v3/orders/{orderNumber} | Returns the contents of the specified order
[**orderManagementGetRefund**](OrderManagementApi.md#orderManagementGetRefund) | **GET** /api/v3/orders/{orderNumber}/refunds/{refundNumber} | Returns all or specific refunds of the order.
[**orderManagementGetShippingDetails**](OrderManagementApi.md#orderManagementGetShippingDetails) | **GET** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details/{shippingNumber} | Returns all or specific shipping details information of the capture.
[**orderManagementGetVoid**](OrderManagementApi.md#orderManagementGetVoid) | **GET** /api/v3/orders/{orderNumber}/voids/{voidNumber} | Returns all or specific voided (cancelled) authorizations of the order.
[**orderManagementRefund**](OrderManagementApi.md#orderManagementRefund) | **POST** /api/v3/orders/{orderNumber}/refunds | Refunds a partially or fully captured payment.
[**orderManagementUpdateShippingDetails**](OrderManagementApi.md#orderManagementUpdateShippingDetails) | **PATCH** /api/v3/orders/{orderNumber}/captures/{captureNumber}/shipping-details/{shippingNumber} | Update shipping details information of the specific capture.
[**orderManagementVoid**](OrderManagementApi.md#orderManagementVoid) | **POST** /api/v3/orders/{orderNumber}/voids | Void (cancel) an authorization that has not been captured.


# **orderManagementAddShippingDetails**
> \Trollweb\AfterPayApi\Model\AddShippingDetailsResponse orderManagementAddShippingDetails($order_number, $capture_number, $request)

Add new shipping details information to the capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$capture_number = "capture_number_example"; // string | Capture number
$request = new \Trollweb\AfterPayApi\Model\AddShippingDetailsRequest(); // \Trollweb\AfterPayApi\Model\AddShippingDetailsRequest | Request object

try {
    $result = $api_instance->orderManagementAddShippingDetails($order_number, $capture_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementAddShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **capture_number** | **string**| Capture number |
 **request** | [**\Trollweb\AfterPayApi\Model\AddShippingDetailsRequest**](../Model/AddShippingDetailsRequest.md)| Request object |

### Return type

[**\Trollweb\AfterPayApi\Model\AddShippingDetailsResponse**](../Model/AddShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementCapture**
> \Trollweb\AfterPayApi\Model\CaptureResponse orderManagementCapture($order_number, $request)

Completes the payment that has been authorized. Typically done when the order is shipped. Can be a full or partial capture of the order amount.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$request = new \Trollweb\AfterPayApi\Model\CaptureRequest(); // \Trollweb\AfterPayApi\Model\CaptureRequest | Request object

try {
    $result = $api_instance->orderManagementCapture($order_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementCapture: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **request** | [**\Trollweb\AfterPayApi\Model\CaptureRequest**](../Model/CaptureRequest.md)| Request object | [optional]

### Return type

[**\Trollweb\AfterPayApi\Model\CaptureResponse**](../Model/CaptureResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementDeleteShippingDetails**
> \Trollweb\AfterPayApi\Model\DeleteShippingDetailsResponse orderManagementDeleteShippingDetails($order_number, $capture_number, $shipping_number)

Delete shipping details information from the specific capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$capture_number = "capture_number_example"; // string | Capture number
$shipping_number = 56; // int | Shipping number

try {
    $result = $api_instance->orderManagementDeleteShippingDetails($order_number, $capture_number, $shipping_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementDeleteShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **capture_number** | **string**| Capture number |
 **shipping_number** | **int**| Shipping number |

### Return type

[**\Trollweb\AfterPayApi\Model\DeleteShippingDetailsResponse**](../Model/DeleteShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetCapture**
> \Trollweb\AfterPayApi\Model\GetAllCapturesResponse orderManagementGetCapture($order_number, $capture_number)

Returns all or specific captured payments of the order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$capture_number = "capture_number_example"; // string | Capture number

try {
    $result = $api_instance->orderManagementGetCapture($order_number, $capture_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetCapture: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **capture_number** | **string**| Capture number |

### Return type

[**\Trollweb\AfterPayApi\Model\GetAllCapturesResponse**](../Model/GetAllCapturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetOrder**
> \Trollweb\AfterPayApi\Model\GetOrderResponse orderManagementGetOrder($order_number)

Returns the contents of the specified order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number

try {
    $result = $api_instance->orderManagementGetOrder($order_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |

### Return type

[**\Trollweb\AfterPayApi\Model\GetOrderResponse**](../Model/GetOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetRefund**
> \Trollweb\AfterPayApi\Model\GetAllRefundsResponse orderManagementGetRefund($order_number, $refund_number)

Returns all or specific refunds of the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$refund_number = "refund_number_example"; // string | Refund number

try {
    $result = $api_instance->orderManagementGetRefund($order_number, $refund_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **refund_number** | **string**| Refund number |

### Return type

[**\Trollweb\AfterPayApi\Model\GetAllRefundsResponse**](../Model/GetAllRefundsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetShippingDetails**
> \Trollweb\AfterPayApi\Model\GetAllShippingDetailsResponse orderManagementGetShippingDetails($order_number, $capture_number, $shipping_number)

Returns all or specific shipping details information of the capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$capture_number = "capture_number_example"; // string | Capture number
$shipping_number = 56; // int | Shipping number

try {
    $result = $api_instance->orderManagementGetShippingDetails($order_number, $capture_number, $shipping_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **capture_number** | **string**| Capture number |
 **shipping_number** | **int**| Shipping number |

### Return type

[**\Trollweb\AfterPayApi\Model\GetAllShippingDetailsResponse**](../Model/GetAllShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementGetVoid**
> \Trollweb\AfterPayApi\Model\GetVoidsResponse orderManagementGetVoid($order_number, $void_number)

Returns all or specific voided (cancelled) authorizations of the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$void_number = "void_number_example"; // string | Void number

try {
    $result = $api_instance->orderManagementGetVoid($order_number, $void_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementGetVoid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **void_number** | **string**| Void number |

### Return type

[**\Trollweb\AfterPayApi\Model\GetVoidsResponse**](../Model/GetVoidsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementRefund**
> \Trollweb\AfterPayApi\Model\RefundOrderResponse orderManagementRefund($order_number, $request)

Refunds a partially or fully captured payment.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$request = new \Trollweb\AfterPayApi\Model\RefundOrderRequest(); // \Trollweb\AfterPayApi\Model\RefundOrderRequest | Request object

try {
    $result = $api_instance->orderManagementRefund($order_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **request** | [**\Trollweb\AfterPayApi\Model\RefundOrderRequest**](../Model/RefundOrderRequest.md)| Request object | [optional]

### Return type

[**\Trollweb\AfterPayApi\Model\RefundOrderResponse**](../Model/RefundOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementUpdateShippingDetails**
> \Trollweb\AfterPayApi\Model\UpdateShippingDetailsResponse orderManagementUpdateShippingDetails($order_number, $capture_number, $shipping_number, $request)

Update shipping details information of the specific capture.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$capture_number = "capture_number_example"; // string | Capture number
$shipping_number = 56; // int | Shipping number
$request = new \Trollweb\AfterPayApi\Model\UpdateShippingDetailsRequest(); // \Trollweb\AfterPayApi\Model\UpdateShippingDetailsRequest | Request object

try {
    $result = $api_instance->orderManagementUpdateShippingDetails($order_number, $capture_number, $shipping_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementUpdateShippingDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **capture_number** | **string**| Capture number |
 **shipping_number** | **int**| Shipping number |
 **request** | [**\Trollweb\AfterPayApi\Model\UpdateShippingDetailsRequest**](../Model/UpdateShippingDetailsRequest.md)| Request object |

### Return type

[**\Trollweb\AfterPayApi\Model\UpdateShippingDetailsResponse**](../Model/UpdateShippingDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderManagementVoid**
> \Trollweb\AfterPayApi\Model\VoidAuthorizationResponse orderManagementVoid($order_number, $request)

Void (cancel) an authorization that has not been captured.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\OrderManagementApi();
$order_number = "order_number_example"; // string | Order number
$request = new \Trollweb\AfterPayApi\Model\VoidAuthorizationRequest(); // \Trollweb\AfterPayApi\Model\VoidAuthorizationRequest | Request

try {
    $result = $api_instance->orderManagementVoid($order_number, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->orderManagementVoid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Order number |
 **request** | [**\Trollweb\AfterPayApi\Model\VoidAuthorizationRequest**](../Model/VoidAuthorizationRequest.md)| Request | [optional]

### Return type

[**\Trollweb\AfterPayApi\Model\VoidAuthorizationResponse**](../Model/VoidAuthorizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

