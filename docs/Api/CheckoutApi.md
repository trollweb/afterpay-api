# Trollweb\AfterPayApi\CheckoutApi

All URIs are relative to *https://api.afterpay.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkoutAuthorizePayment**](CheckoutApi.md#checkoutAuthorizePayment) | **POST** /api/v3/checkout/authorize | Approves the payment for a specified customer and basket. Main use-cases are One-Step and Two-Step Authorization.  Full fraud and credit scoring applied.  If the online-shop customer enter the incorrect address data, the system delivers a suggestion for a corrected address.
[**checkoutAvailablePaymentMethods**](CheckoutApi.md#checkoutAvailablePaymentMethods) | **POST** /api/v3/checkout/payment-methods | Returns the available payment methods for a specified basket value and offers available for client.  Returns monthly installment amount, interest and fees.  Sending in customer data and contents of the basket to enhance the result is optional.  If the online-shop customer enters the incorrect address data, the system delivers suggestion for a corrected address.  Main use-case is Two-Step Authorize.
[**checkoutCreateContract**](CheckoutApi.md#checkoutCreateContract) | **POST** /api/v3/checkout/{checkoutId}/contract | Creates an installment and/or direct debit contract that can be displayed for the customer.


# **checkoutAuthorizePayment**
> \Trollweb\AfterPayApi\Model\AuthorizePaymentResponse checkoutAuthorizePayment($request)

Approves the payment for a specified customer and basket. Main use-cases are One-Step and Two-Step Authorization.  Full fraud and credit scoring applied.  If the online-shop customer enter the incorrect address data, the system delivers a suggestion for a corrected address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CheckoutApi();
$request = new \Trollweb\AfterPayApi\Model\AuthorizePaymentRequest(); // \Trollweb\AfterPayApi\Model\AuthorizePaymentRequest | 

try {
    $result = $api_instance->checkoutAuthorizePayment($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->checkoutAuthorizePayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\AuthorizePaymentRequest**](../Model/AuthorizePaymentRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\AuthorizePaymentResponse**](../Model/AuthorizePaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **checkoutAvailablePaymentMethods**
> \Trollweb\AfterPayApi\Model\AvailablePaymentMethodsResponse checkoutAvailablePaymentMethods($request)

Returns the available payment methods for a specified basket value and offers available for client.  Returns monthly installment amount, interest and fees.  Sending in customer data and contents of the basket to enhance the result is optional.  If the online-shop customer enters the incorrect address data, the system delivers suggestion for a corrected address.  Main use-case is Two-Step Authorize.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CheckoutApi();
$request = new \Trollweb\AfterPayApi\Model\AvailablePaymentMethodsRequest(); // \Trollweb\AfterPayApi\Model\AvailablePaymentMethodsRequest | 

try {
    $result = $api_instance->checkoutAvailablePaymentMethods($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->checkoutAvailablePaymentMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\Trollweb\AfterPayApi\Model\AvailablePaymentMethodsRequest**](../Model/AvailablePaymentMethodsRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\AvailablePaymentMethodsResponse**](../Model/AvailablePaymentMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **checkoutCreateContract**
> \Trollweb\AfterPayApi\Model\CreateContractResponse checkoutCreateContract($checkout_id, $request)

Creates an installment and/or direct debit contract that can be displayed for the customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\AfterPayApi\Api\CheckoutApi();
$checkout_id = "checkout_id_example"; // string | 
$request = new \Trollweb\AfterPayApi\Model\CreateContractRequest(); // \Trollweb\AfterPayApi\Model\CreateContractRequest | 

try {
    $result = $api_instance->checkoutCreateContract($checkout_id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->checkoutCreateContract: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_id** | **string**|  |
 **request** | [**\Trollweb\AfterPayApi\Model\CreateContractRequest**](../Model/CreateContractRequest.md)|  |

### Return type

[**\Trollweb\AfterPayApi\Model\CreateContractResponse**](../Model/CreateContractResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

