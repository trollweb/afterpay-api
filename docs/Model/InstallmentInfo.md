# InstallmentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basket_amount** | **double** | Basket amount | [optional] 
**number_of_installments** | **int** | Number of installments | [optional] 
**installment_amount** | **double** | Monthly installment amount | [optional] 
**first_installment_amount** | **double** | Amount of the first installment payment | [optional] 
**last_installment_amount** | **double** | Amount of the last installment payment | [optional] 
**interest_rate** | **double** | Interest rate | [optional] 
**effective_interest_rate** | **double** | Effective interest rate | [optional] 
**effective_annual_percentage_rate** | **double** | Effective annual percentage rate | [optional] 
**total_interest_amount** | **double** | Total interest amount | [optional] 
**startup_fee** | **double** | Fee for opening up an installment plan | [optional] 
**monthly_fee** | **double** | Monthly fee for the installment amount | [optional] 
**total_amount** | **double** | Total amount | [optional] 
**installment_profile_number** | **int** | Installment profile number | [optional] 
**read_more** | **string** | More information on installment process | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


