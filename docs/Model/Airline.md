# Airline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passengers** | [**\Trollweb\AfterPayApi\Model\Passenger[]**](Passenger.md) | Passengers | [optional] 
**itineraries** | [**\Trollweb\AfterPayApi\Model\Itinerary[]**](Itinerary.md) | Itineraties | [optional] 
**insurance** | [**\Trollweb\AfterPayApi\Model\Insurance**](Insurance.md) | Insurance | [optional] 
**booking_reference** | **string** | Booking reference | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


