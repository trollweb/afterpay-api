# OrderRisk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_type** | **string** | Order channel type | [optional] 
**delivery_type** | **string** | Order delivery type | [optional] 
**ticket_delivery_method** | **string** | Name of the tickets&#39; method | [optional] 
**airline** | [**\Trollweb\AfterPayApi\Model\Airline**](Airline.md) | Airline information when airline tickets or services are ordered | [optional] 
**bus** | [**\Trollweb\AfterPayApi\Model\Bus**](Bus.md) | Bus information when bus tickets or services are ordered | [optional] 
**train** | [**\Trollweb\AfterPayApi\Model\Train**](Train.md) | Train information when train tickets or services are ordered | [optional] 
**ferry** | [**\Trollweb\AfterPayApi\Model\Ferry**](Ferry.md) | Ferry information when ferry tickets or services are ordered | [optional] 
**rental** | [**\Trollweb\AfterPayApi\Model\Rental**](Rental.md) | Rental car information | [optional] 
**hotel** | [**\Trollweb\AfterPayApi\Model\Hotel**](Hotel.md) | Hotel information | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


