# References

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**your_reference** | **string** | Order reference in your system | [optional] 
**our_reference** | **string** | Order reference in our system | [optional] 
**merchant_id** | **string** | Merchant ID | [optional] 
**contract_date** | [**\DateTime**](\DateTime.md) | Contract Date | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) | Invoice Date | [optional] 
**costcenter** | **string** | Costcenter (used for B2B orders) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


