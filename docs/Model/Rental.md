# Rental

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** | Name of rental company | [optional] 
**pickup_location** | [**\Trollweb\AfterPayApi\Model\Address**](Address.md) | Pickup location | [optional] 
**dropoff_location** | [**\Trollweb\AfterPayApi\Model\Address**](Address.md) | Drop off location | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Pick up date and time | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | End date and time | [optional] 
**drivers** | [**\Trollweb\AfterPayApi\Model\CheckoutCustomer[]**](CheckoutCustomer.md) | Information about drivers who have rented a car | [optional] 
**price** | **double** | Price of a rental car | [optional] 
**currency** | **string** | Currency | [optional] 
**booking_reference** | **string** | Booking reference | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


