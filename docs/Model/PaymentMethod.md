# PaymentMethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Available payment type | [optional] 
**account** | [**\Trollweb\AfterPayApi\Model\AccountProduct**](AccountProduct.md) | Account product information | [optional] 
**direct_debit** | [**\Trollweb\AfterPayApi\Model\DirectDebitInfo**](DirectDebitInfo.md) | Direct debit availability for this payment type | [optional] 
**campaigns** | [**\Trollweb\AfterPayApi\Model\CampaignInfo[]**](CampaignInfo.md) | Available campaigns for this payment type | [optional] 
**installment** | [**\Trollweb\AfterPayApi\Model\InstallmentInfo**](InstallmentInfo.md) | Installment information when payment type is Installment | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


