# CaptureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_details** | [**\Trollweb\AfterPayApi\Model\OrderSummary**](OrderSummary.md) | Order details | 
**invoice_number** | **string** | Invoice number. The merchant may want to use internal invoice numbers, or let the AferPay generate one at random.  This will show up in AfterPay&#39;s portal. One order may have one or more invoices. | [optional] 
**campaign_number** | **int** | Invoice campaign number. The merchant may link multiple orders to a campaign they are running. | [optional] 
**shipping_details** | [**\Trollweb\AfterPayApi\Model\ShippingDetails[]**](ShippingDetails.md) | Shipping information which includes shipping type, company and tracking ID | [optional] 
**parent_transaction_reference** | **string** | parentTransactionReference is a unique reference from merchant or PSP system that can be used for future reference by the merchants | [optional] 
**references** | [**\Trollweb\AfterPayApi\Model\References**](References.md) | References | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


