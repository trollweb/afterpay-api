# AddTransactionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_type** | **string** | Transaction type | 
**amount** | **double** | Transaction amount | 
**currency** | **string** | Transaction currency | 
**account_transaction_id** | **string** | Transaction ID | [optional] 
**additional_transaction_info** | [**\Trollweb\AfterPayApi\Model\AdditionalTransactionInfo**](AdditionalTransactionInfo.md) | Additional transaction info | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


