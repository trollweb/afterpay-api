# ValidateBankAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_account** | **string** | Account number | 
**bank_code** | **string** | Account swift number | [optional] 
**bank_number** | **string** | Bank and branch identifier | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


