# Subscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name / Description of the subscription | [optional] 
**start** | [**\DateTime**](\DateTime.md) | Startdate of the subscription | [optional] 
**end** | [**\DateTime**](\DateTime.md) | Enddate of the subscription | [optional] 
**type** | **string** | Information if subscription will be autorenewed if not canceled or if subscription is for a fixed period (e.g. 6 months) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


