# RefundOrderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capture_number** | **string** | Capture number to refund. This is the capture number which comes from Capture response | 
**order_items** | [**\Trollweb\AfterPayApi\Model\RefundOrderItem[]**](RefundOrderItem.md) | Order items to refund | 
**refund_type** | **string** | Type of the refund. Please use \&quot;Return\&quot; for returned items and \&quot;Refund\&quot; for goodwill refunds. | [optional] 
**parent_transaction_reference** | **string** | parentTransactionReference is a unique reference from merchant or PSP system that can be used for future reference by the merchants | [optional] 
**credit_note_number** | **string** | Credit note number. The merchant may want to use internal credit note numbers, or let the AferPay generate one based on invoice to be credited. | [optional] 
**merchant_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


