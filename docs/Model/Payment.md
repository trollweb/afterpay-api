# Payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Payment method | 
**contract_id** | **string** | Contract ID | [optional] 
**direct_debit** | [**\Trollweb\AfterPayApi\Model\DirectDebit**](DirectDebit.md) | Direct debit information | [optional] 
**campaign** | [**\Trollweb\AfterPayApi\Model\Campaign**](Campaign.md) | Campaign information | [optional] 
**invoice** | [**\Trollweb\AfterPayApi\Model\Invoice**](Invoice.md) | Invoice information. Reserved for future use. | [optional] 
**account** | [**\Trollweb\AfterPayApi\Model\AccountProduct**](AccountProduct.md) | Used when consumer has chosen flexible installment plan (minimum amount to pay) | [optional] 
**consolidated_invoice** | [**\Trollweb\AfterPayApi\Model\ConsolidatedInvoice**](ConsolidatedInvoice.md) | Consolidate invoice information | [optional] 
**installment** | [**\Trollweb\AfterPayApi\Model\Installment**](Installment.md) | Installment information | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


