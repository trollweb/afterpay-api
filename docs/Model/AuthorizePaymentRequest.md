# AuthorizePaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkout_id** | **string** | Unique identifier of checkout process in UUID format. Required only in the Two-Step Authorize use-case. | [optional] 
**merchant_id** | **string** | Merchant ID | [optional] 
**payment** | [**\Trollweb\AfterPayApi\Model\Payment**](Payment.md) | Default payment method | 
**customer** | [**\Trollweb\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | CheckoutCustomer object | [optional] 
**delivery_customer** | [**\Trollweb\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | To be used if the reciever of the order differs from the billing customer | [optional] 
**order** | [**\Trollweb\AfterPayApi\Model\Order**](Order.md) | Order object | [optional] 
**parent_transaction_reference** | **string** | parentTransactionReference is a unique reference from merchant or PSP system that can be used for future reference by the merchants | [optional] 
**additional_data** | [**\Trollweb\AfterPayApi\Model\AdditionalData**](AdditionalData.md) | Additional data | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


