# AvailableInstallmentPlansResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available_installment_plans** | [**\Trollweb\AfterPayApi\Model\InstallmentInfo[]**](InstallmentInfo.md) | Available installment plans | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


