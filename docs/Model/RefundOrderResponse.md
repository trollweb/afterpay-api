# RefundOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_captured_amount** | **double** | The total captured amount done with either a full capture, partial capture, or multiple partial captures | [optional] 
**total_authorized_amount** | **double** | Total authorized amount | [optional] 
**refund_numbers** | **string[]** | RefundNumber is assigned to an invoice that has been refunded. For one Authorization you can have multiple invoices and refundNumbers | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


