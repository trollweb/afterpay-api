# UserProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** | First name | [optional] 
**last_name** | **string** | Last name | [optional] 
**mobile_number** | **string** | Mobile number | [optional] 
**e_mail** | **string** | Email address | [optional] 
**language_code** | **string** | Language code | [optional] 
**address_list** | [**\Trollweb\AfterPayApi\Model\LookupAddress[]**](LookupAddress.md) | Array of LookupAddress objects | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


