# AdditionalData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_provider** | **string** | Plugin Provider Name | [optional] 
**plugin_version** | **string** | Plugin Version | [optional] 
**shop_url** | **string** | URL of the Webshop | [optional] 
**shop_platform** | **string** | Name of the platform used for the Webshop | [optional] 
**shop_platform_version** | **string** | Version of the webshop platform | [optional] 
**marketplace** | [**\Trollweb\AfterPayApi\Model\Marketplace[]**](Marketplace.md) | Additional information for marketplace set-ups | [optional] 
**subscription** | [**\Trollweb\AfterPayApi\Model\Subscription**](Subscription.md) | Additional information for subscription businesses | [optional] 
**partner_data** | [**\Trollweb\AfterPayApi\Model\PartnerData**](PartnerData.md) | Additional data to be provided by PSP or platform provider | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


