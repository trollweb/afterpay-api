# ApplyForAccountCreditRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_limit** | **double** | Account limit | 
**customer** | [**\Trollweb\AfterPayApi\Model\Customer**](Customer.md) | Customer linked with this account | 
**first_notice_date** | [**\DateTime**](\DateTime.md) | First notice date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


