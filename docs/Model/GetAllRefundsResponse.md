# GetAllRefundsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refunds** | [**\Trollweb\AfterPayApi\Model\Refund[]**](Refund.md) | Collection of refunds | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


