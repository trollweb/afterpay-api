# ShippingDetailsWithNumber

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_number** | **int** | Unique shipping number within one capture | 
**type** | **string** | Shipping type | 
**shipping_company** | **string** | Company name providing shipping | 
**tracking_id** | **string** | Tracking ID | 
**tracking_url** | **string** | Webpage URL to track shipping status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


