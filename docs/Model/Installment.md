# Installment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile_no** | **int** | Installment profile number | 
**number_of_installments** | **int** | Number of installments | [optional] 
**customer_interest_rate** | **double** | Customer interest rate | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


