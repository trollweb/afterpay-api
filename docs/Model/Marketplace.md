# Marketplace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID of the marketplace seller | [optional] 
**name** | **string** | Name of the marketplace seller | [optional] 
**registered_since** | [**\DateTime**](\DateTime.md) | Registration date of the marketplace seller | [optional] 
**rating** | **string** | Rating of the marketplace seller | [optional] 
**transactions** | **int** | Number of transaction processed by the marketplace seller | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


