# CustomerRisk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**existing_customer** | **bool** | Is customer an existing customer for merchant | [optional] 
**verified_customer_identification** | **bool** | Is identification verified | [optional] 
**marketing_opt_in** | **bool** | Shows if merchant is allowed to send marketing information to customer | [optional] 
**customer_since** | [**\DateTime**](\DateTime.md) | Since when customer has been merchant&#39;s client | [optional] 
**customer_classification** | **string** | Customer reputation (e.g. VIP client) | [optional] 
**acquisition_channel** | **string** | Specify the channel which consumer has used for accessing merchant page | [optional] 
**has_customer_card** | **bool** | Shows if customer has loyalty card | [optional] 
**customer_card_since** | [**\DateTime**](\DateTime.md) | Customer card since | [optional] 
**customer_card_classification** | **string** | Specifies the level of the loyalty card (e.g Gold member) | [optional] 
**profile_tracking_id** | **string** | Unique Id of the device for profile tracking | [optional] 
**ip_address** | **string** | Customer’s IP address | [optional] 
**number_of_transactions** | **int** | Total number of successful purchases that have been made by the specific consumer | [optional] 
**customer_individual_score** | **int** | Individual score provided by the merchant | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


