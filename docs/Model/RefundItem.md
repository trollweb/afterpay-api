# RefundItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refund_id** | **string** | Refund ID | [optional] 
**product_id** | **string** | Merchant Product identification number | 
**group_id** | **string** | Item group ID. The group this item belongs to. Provided by the merchant | [optional] 
**description** | **string** | Product name. For example \&quot;Black music player 64GB\&quot; | 
**type** | **string** |  | [optional] 
**net_unit_price** | **double** | Net unit price | 
**gross_unit_price** | **double** | Gross price per item | 
**quantity** | **double** | Quantity. Use of integer is strongly proposed. If you want to use decimal, please contact your integration manager. | 
**unit_code** | **string** | Unit code (for example pieces, liters, kilograms, etc.) | [optional] 
**vat_category** | **string** | VAT category | [optional] 
**vat_percent** | **double** | Tax percent | 
**vat_amount** | **double** | Tax amount per item | 
**image_url** | **string** | Order item image URL. Image of the specific order item, can be seen on the invoice in MyAfterPay portal.   The merchant can provide a working URL for a square or rectangle image. Size constraints: 100-1280px | [optional] 
**google_product_category_id** | **int** | Google product category ID | [optional] 
**google_product_category** | **string** | Indicates the category of the item based on the Google product taxonomy. Categorizing the product helps ensure that the ad is shown with the right search results | [optional] 
**merchant_product_type** | **string** | Categorization used by Merchant as a complement to Google Taxonomy | [optional] 
**line_number** | **int** | Line number. The merchant may add a line number to each order item, to sort them in a particular order | [optional] 
**product_url** | **string** | URL to the product | [optional] 
**market_place_seller_id** | **string** | ID of an individual seller on a marketplace | [optional] 
**parent_transaction_reference** | **string** |  | [optional] 
**additional_information** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


