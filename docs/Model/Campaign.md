# Campaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_number** | **int** | Campaign number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


