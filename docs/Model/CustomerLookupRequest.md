# CustomerLookupRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Customer email address | [optional] 
**mobile_phone** | **string** | Phone number | [optional] 
**country_code** | **string** | Country code | [optional] 
**postal_code** | **string** | Postal code | [optional] 
**identification_number** | **string** | Personal identification number (SSN) | [optional] 
**customer_number** | **string** | Customer number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


