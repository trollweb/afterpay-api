# ValidateBankAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_valid** | **bool** | Is response valid | [optional] 
**risk_check_messages** | [**\Trollweb\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


