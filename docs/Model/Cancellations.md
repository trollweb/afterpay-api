# Cancellations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancellation_no** | **string** | Order number | [optional] 
**cancellation_amount** | **double** | Amount of the cancelled items | [optional] 
**cancellation_items** | [**\Trollweb\AfterPayApi\Model\CancellationItem[]**](CancellationItem.md) | The list of items to be cancelled | [optional] 
**parent_transaction_reference** | **string** | Reference submitted by PSP | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


