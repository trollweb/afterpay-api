# ResponseMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Message type | [optional] 
**code** | **string** | Confirmation or error code | [optional] 
**message** | **string** | Message content | [optional] 
**customer_facing_message** | **string** | Message to display to customer | [optional] 
**action_code** | **string** | Possible next action to make | [optional] 
**field_reference** | **string** | Reference to field that caused an error | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


