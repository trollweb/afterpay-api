# LookupAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** |  | [optional] 
**street2** | **string** |  | [optional] 
**street3** | **string** |  | [optional] 
**street4** | **string** |  | [optional] 
**street_number** | **string** |  | [optional] 
**flat_no** | **string** |  | [optional] 
**entrance** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**postal_code** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**country_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


