# EventItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **string** | Item ID | 
**item_description** | **string** | Item description | 
**unit_code** | **string** | Unit code | 
**quantity** | **double** | Quantity | 
**unit_price** | **double** | Unit price (gross amount) | 
**net_amount** | **double** | Net amount of one item | [optional] 
**tax_amount** | **double** | Tax amount of one item | [optional] 
**discount** | **double** | Total discount | [optional] 
**gross_amount** | **double** | Total gross amount (unit price * quantity - discount) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


