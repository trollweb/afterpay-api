# DirectDebit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank_code** | **string** | Direct debit swift, BIC, SortCode | [optional] 
**bank_account** | **string** | Direct debit bank account | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


