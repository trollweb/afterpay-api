# GetAllShippingDetailsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_details** | [**\Trollweb\AfterPayApi\Model\ShippingDetailsWithNumber[]**](ShippingDetailsWithNumber.md) | Collection of shipping details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


