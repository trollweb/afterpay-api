# ResponseOrderDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **string** | Order ID | [optional] 
**order_number** | **string** | Order number | [optional] 
**total_net_amount** | **double** | Total net amount of order | [optional] 
**total_gross_amount** | **double** | Total gross amount of order | [optional] 
**currency** | **string** | Order currency | [optional] 
**order_channel_type** | **string** | Order channel type | [optional] 
**order_delivery_type** | **string** | Order delivery type | [optional] 
**has_separate_delivery_address** | **bool** | Indicates whether order has separate delivery address or not | [optional] 
**customer** | [**\Trollweb\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | Customer information | [optional] 
**inserted_at** | [**\DateTime**](\DateTime.md) | Indicates the creation of the specific element (e.g. orderDetails) | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Indicates the time of the latest change of the element | [optional] 
**image_url** | **string** | Order image URL. Overview image of the invoice in MyAfterPay portal.   The merchant can provide a working URL for a square or rectangle image. Size constraints: 100-1280px | [optional] 
**merchant_image_url** | **string** | Image URL for the merchants brand. This image is shown at the top of the order page in MyAfterPay | [optional] 
**google_analytics_user_id** | **string** | Google Analytics User ID | [optional] 
**order_items** | [**\Trollweb\AfterPayApi\Model\OrderItemExtended[]**](OrderItemExtended.md) | Order items | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


