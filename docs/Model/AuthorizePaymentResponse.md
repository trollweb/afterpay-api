# AuthorizePaymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outcome** | **string** | Outcome | [optional] 
**customer** | [**\Trollweb\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer | [optional] 
**delivery_customer** | [**\Trollweb\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | To be used if the reciever of the order differs from the billing customer | [optional] 
**reservation_id** | **string** |  | [optional] 
**checkout_id** | **string** |  | [optional] 
**risk_check_messages** | [**\Trollweb\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


