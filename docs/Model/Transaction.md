# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_id** | **int** |  | [optional] 
**amount** | **double** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transaction_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transaction_type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


