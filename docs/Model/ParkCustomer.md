# ParkCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age** | **int** | Customer age | 
**gender** | **string** | Customer gender | 
**height** | **int** | Customer height | 
**salutation** | **string** | Salutation. | [optional] 
**first_name** | **string** | First name | [optional] 
**last_name** | **string** | Last name | [optional] 
**customer_number** | **string** | Customer number | [optional] 
**organization_personal_no** | **string** | Organization (if customer is organization) or personal (if customer is person) identification number | [optional] 
**category** | **string** | Customer category. | 
**currency** | **string** | Customer currency | 
**distribution_by** | **string** | Specifies who will distribute information (invoices, notices, etc.) to customer | [optional] 
**distribution_type** | **string** | Specifies how information (invoices, notices, etc.) will be distributed to customer | [optional] 
**email** | **string** | Customer email | [optional] 
**address** | [**\Trollweb\AfterPayApi\Model\Address**](Address.md) | Customer address details | [optional] 
**phone** | **string** | Customer&#39;s primary phone | [optional] 
**direct_phone** | **string** | Customer&#39;s direct phone number (land line) | [optional] 
**mobile_phone** | **string** | Customer&#39;s mobile phone | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


