# Refund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refund_id** | **string** | Refund ID | [optional] 
**reservation_id** | **string** | Reservation ID | [optional] 
**customer_number** | **string** | Customer number | [optional] 
**refund_number** | **string** | Refund number | [optional] 
**order_number** | **string** | Order number | [optional] 
**amount** | **double** | Amount | [optional] 
**balance** | **double** | Balance | [optional] 
**currency** | **string** | Currency code | [optional] 
**inserted_at** | [**\DateTime**](\DateTime.md) | Inserted at | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Updated at | [optional] 
**capture_number** | **string** | Capture number | [optional] 
**refund_items** | [**\Trollweb\AfterPayApi\Model\RefundItem[]**](RefundItem.md) | Refund items | [optional] 
**parent_transaction_reference** | **string** | Reference submitted by PSP | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


