# Contract

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract_type** | **string** | Contract type | [optional] 
**contract_content** | **string** | Contract content | [optional] 
**contract_number** | **string** | Contract number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


