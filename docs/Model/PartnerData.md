# PartnerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**psp_name** | **string** | Name of the PSP | [optional] 
**psp_type** | **string** | Type of the PSP | [optional] 
**tracking_provider** | **string** | If device fingerprinting is applied, please specify the name of the provider | [optional] 
**tracking_session_id** | **string** | Session id of tracking script | [optional] 
**tracking_score** | **string** | Score provided by tracking provider | [optional] 
**challenged_score** | **string** | Score provided by partner if transaction is challenged | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


