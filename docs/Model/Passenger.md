# Passenger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Continuous numbering | [optional] 
**first_name** | **string** | First name | [optional] 
**last_name** | **string** | Last name | [optional] 
**salutation** | **string** | Salutation | [optional] 
**date_of_birth** | [**\DateTime**](\DateTime.md) | Date birth of passenger | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


