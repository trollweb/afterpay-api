# CustomerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_number** | **string** | Customer number | [optional] 
**customer_account_id** | **string** | Customer account id | [optional] 
**first_name** | **string** | First name | [optional] 
**last_name** | **string** | Last name | [optional] 
**address_list** | [**\Trollweb\AfterPayApi\Model\Address[]**](Address.md) | Address list | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


