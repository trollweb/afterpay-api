# Capture

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capture_id** | **string** | Capture ID | [optional] 
**reservation_id** | **string** | Reservation ID | [optional] 
**customer_number** | **string** | Customer number | [optional] 
**capture_number** | **string** | Capture number | [optional] 
**order_number** | **string** | Order number | [optional] 
**amount** | **double** | Amount | [optional] 
**balance** | **double** | Balance | [optional] 
**total_refunded_amount** | **double** | Total refunded amount | [optional] 
**currency** | **string** | Currency code | [optional] 
**inserted_at** | [**\DateTime**](\DateTime.md) | Inserted at | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Updated at | [optional] 
**direct_debit_bank_account** | **string** | Direct debit bank account | [optional] 
**direct_debit_swift** | **string** | Direct debit swift | [optional] 
**account_profile_number** | **int** | Account profile number | [optional] 
**number_of_installments** | **int** | Number of installments | [optional] 
**installment_amount** | **double** | Installment amount | [optional] 
**contract_date** | [**\DateTime**](\DateTime.md) | Contract date | [optional] 
**order_date** | [**\DateTime**](\DateTime.md) | Order date | [optional] 
**installment_profile_number** | **int** | Installment profile number | [optional] 
**parent_transaction_reference** | **string** | Reference submitted by PSP | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) | Due date | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) | Invoice date | [optional] 
**your_reference** | **string** | Your reference | [optional] 
**our_reference** | **string** | Our reference | [optional] 
**invoice_profile_number** | **int** | Invoice profile number | [optional] 
**ocr** | **string** | OCR (Optical Character Recognition) number bound to this capture | [optional] 
**installment_customer_interest_rate** | **double** | Installment customer interest rate | [optional] 
**image_url** | **string** | Image Url | [optional] 
**capture_items** | [**\Trollweb\AfterPayApi\Model\CaptureItem[]**](CaptureItem.md) | Capture items | [optional] 
**shipping_details** | [**\Trollweb\AfterPayApi\Model\ShippingDetailsWithNumber[]**](ShippingDetailsWithNumber.md) | Shipping details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


