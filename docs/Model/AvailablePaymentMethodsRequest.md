# AvailablePaymentMethodsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent_transaction_reference** | **string** | Reference submitted by PSP | [optional] 
**additional_data** | [**\Trollweb\AfterPayApi\Model\AdditionalData**](AdditionalData.md) | Additional data | [optional] 
**customer** | [**\Trollweb\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | Customer | 
**delivery_customer** | [**\Trollweb\AfterPayApi\Model\CheckoutCustomer**](CheckoutCustomer.md) | Delivery customer | [optional] 
**order** | [**\Trollweb\AfterPayApi\Model\Order**](Order.md) | Order number and details | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


