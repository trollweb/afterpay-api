# AddEventToAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_added** | **bool** | Indicates whether event was added or not | [optional] 
**transaction_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


