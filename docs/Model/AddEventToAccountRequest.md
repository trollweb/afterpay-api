# AddEventToAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_type** | **string** | Event type | 
**category_type** | **string** | Category type | [optional] 
**time** | [**\DateTime**](\DateTime.md) | Date and time when event has appeared | 
**nfc_id** | **string** | ID of NFC chip used in event | [optional] 
**pos_id** | **string** | ID of POS terminal used in event | [optional] 
**pos_name** | **string** | Name of POS terminal used in event | [optional] 
**total_tax** | **double** | Total tax for this event (sum of all taxes of linked items) | [optional] 
**total_amount** | **double** | Total amount for this event (sum of all gross amounts of linked items) | [optional] 
**items** | [**\Trollweb\AfterPayApi\Model\EventItem[]**](EventItem.md) | Event items linked to this event | [optional] 
**currency** | **string** | Transaction currency | 
**account_transaction_id** | **string** | Transaction ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


