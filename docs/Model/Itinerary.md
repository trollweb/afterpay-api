# Itinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operator** | **string** | Name or Airport code (2-letter IATA) of the carrier | [optional] 
**departure** | **string** | Departure city or Airport Code (3-letter IATA) | [optional] 
**arrival** | **string** | Arrival city or Airport Code (3-letter IATA) | [optional] 
**route_number** | **string** | Number of this itinerary. Assumes String ordering. | [optional] 
**date_of_travel** | [**\DateTime**](\DateTime.md) | Departure date and time | [optional] 
**price** | **double** | Price | [optional] 
**currency** | **string** | Currency | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


