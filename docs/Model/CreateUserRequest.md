# CreateUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_id** | **int** | Company ID | [optional] 
**customer** | [**\Trollweb\AfterPayApi\Model\ParkCustomer**](ParkCustomer.md) | Park customer details | 
**limit** | **double** | User&#39;s limit in park | 
**nfc_id** | **string** | ID of NFC chip linked with this user | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


