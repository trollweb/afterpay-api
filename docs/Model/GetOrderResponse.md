# GetOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_details** | [**\Trollweb\AfterPayApi\Model\ResponseOrderDetails**](ResponseOrderDetails.md) | Order details | [optional] 
**captures** | [**\Trollweb\AfterPayApi\Model\Capture[]**](Capture.md) | Collection of captures | [optional] 
**refunds** | [**\Trollweb\AfterPayApi\Model\Refund[]**](Refund.md) | Collection of refunds | [optional] 
**cancellations** | [**\Trollweb\AfterPayApi\Model\Cancellations[]**](Cancellations.md) | Cancellation info | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


