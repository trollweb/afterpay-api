# GetRemainingLimitResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**remaining_credit_limit** | **double** | Remaining customer account credit limit | [optional] 
**balance** | **double** | Current account balance excluding fees and interest | [optional] 
**is_open_for_new_transactions** | **bool** | Field shows if new purchases («transactions») can be added. This indicates if the owner of the account can make more purchases or withdrawals.  If the account is in dunning due to missing payments, the flag will be false | [optional] 
**account_message** | **string** | Customer account message | [optional] 
**total_balance** | **double** | Customer account balance including fees and interest | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


