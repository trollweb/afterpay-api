# VoidAuthorizationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancellation_details** | [**\Trollweb\AfterPayApi\Model\OrderSummary**](OrderSummary.md) | Information that is used when the order needs to be canceled | [optional] 
**parent_transaction_reference** | **string** | parentTransactionReference is a unique reference from merchant or PSP system that can be used for future reference by the merchants | [optional] 
**merchant_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


