# ApplyForAccountCreditResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**\Trollweb\AfterPayApi\Model\Account**](Account.md) | Created account details | [optional] 
**temporary_external_problem** | **bool** | Indicates whether temporary external problem has happed during account creation or not | [optional] 
**customer** | [**\Trollweb\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


