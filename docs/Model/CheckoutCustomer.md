# CheckoutCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_number** | **string** | Customer unique number | [optional] 
**customer_account_id** | **string** | Customer account id | [optional] 
**identification_number** | **string** | Identification number for person, registration number for company. In Sweden the merchant can only fill  this field, the address and other important information is found accordingly. In other countries  this option is not yet available. This field is coupled with customerCategory. | [optional] 
**salutation** | **string** | Salutation. | [optional] 
**first_name** | **string** | First name | [optional] 
**last_name** | **string** | Last name | [optional] 
**email** | **string** | Email | [optional] 
**phone** | **string** | Phone | [optional] 
**mobile_phone** | **string** | Mobile phone | [optional] 
**birth_date** | [**\DateTime**](\DateTime.md) | Date of birth | [optional] 
**customer_category** | **string** | Customer category. | 
**address** | [**\Trollweb\AfterPayApi\Model\Address**](Address.md) | Address | [optional] 
**risk_data** | [**\Trollweb\AfterPayApi\Model\CustomerRisk**](CustomerRisk.md) | Risk related data. Merchants can do external risk checks and provide that information to AfterPay. | [optional] 
**conversation_language** | **string** | Conversation language | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


