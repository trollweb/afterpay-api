# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_limit** | **double** | Amount of granted account limit if account has limit assigned to it | [optional] 
**account_number** | **string** | Customeraccounts identifier | [optional] 
**account_profile** | **string** | Identifier of a specific accountProfile | [optional] 
**account_status** | **string** | Account status | [optional] 
**balance** | **double** | Current ledger balance | [optional] 
**bank_account** | **string** | Bank account, which is linked to this account | [optional] 
**customer_id** | **string** | Customer ID, which is linked to this account | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Date and time when account was created | [optional] 
**is_stopped** | **bool** | Indicates whether account is stopped or not | [optional] 
**ocr** | **string** | OCR (Optical Character Recognition) number bound to this account | [optional] 
**last_notice_date** | [**\DateTime**](\DateTime.md) | Date of last notice | [optional] 
**next_notice_date** | [**\DateTime**](\DateTime.md) | Date of next notice | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


