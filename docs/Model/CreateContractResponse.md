# CreateContractResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract_id** | **string** | Contract ID | [optional] 
**require_customer_confirmation** | **bool** | RequireCustomerConfirmation | [optional] 
**contract_list** | [**\Trollweb\AfterPayApi\Model\Contract[]**](Contract.md) | Contract list | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


