# OrderSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_net_amount** | **double** | Total net amount of order. | 
**total_gross_amount** | **double** | Total gross amount of order. Must match the grossUnitPrice * quantity of order items | 
**currency** | **string** | Order currency. If not set it will be taken from client&#39;s source system currency setting | [optional] 
**risk** | [**\Trollweb\AfterPayApi\Model\OrderRisk**](OrderRisk.md) | Risk data | [optional] 
**merchant_image_url** | **string** | Image URL for the merchants brand. This image is shown at the top of the order page in MyAfterPay | [optional] 
**image_url** | **string** | Order image URL. Overview image of the invoice in MyAfterPay portal.The merchant can provide a working URL for a square or rectangle image.   Size constraints: 100-1280px | [optional] 
**google_analytics_user_id** | **string** | User ID identifies a user that may interact with content using different browser instances and devices | [optional] 
**google_analytics_client_id** | **string** | Client ID is the identifier used by Google Analytics to represent a browser instance or device | [optional] 
**items** | [**\Trollweb\AfterPayApi\Model\OrderItem[]**](OrderItem.md) | Array of order items | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


