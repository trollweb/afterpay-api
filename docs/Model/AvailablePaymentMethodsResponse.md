# AvailablePaymentMethodsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**risk_check_messages** | [**\Trollweb\AfterPayApi\Model\ResponseMessage[]**](ResponseMessage.md) | Risk check messages | [optional] 
**delivery_customer** | [**\Trollweb\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Delivery customer | [optional] 
**checkout_id** | **string** | Unique identifier of checkout process in UUID format. | [optional] 
**outcome** | **string** | Outcome | [optional] 
**customer** | [**\Trollweb\AfterPayApi\Model\CustomerResponse**](CustomerResponse.md) | Customer | [optional] 
**payment_methods** | [**\Trollweb\AfterPayApi\Model\PaymentMethod[]**](PaymentMethod.md) | Allowed payment methods | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


