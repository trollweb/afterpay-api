# AccountProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile_no** | **int** | Account Profile number | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


