# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** | Name of street or packstation | [optional] 
**street_number** | **string** | Street or packstation number | [optional] 
**street_number_additional** | **string** | Additional street number | [optional] 
**postal_code** | **string** | Postal code | [optional] 
**postal_place** | **string** | Postal place | [optional] 
**country_code** | **string** | Country code | [optional] 
**address_type** | **string** | Address type | [optional] 
**care_of** | **string** | Care of. Intermediary who is responsible for transferring a piece of mail between the postal system and the final addressee. For example Jane c/o John (“Jane at John&#39;s address”) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


