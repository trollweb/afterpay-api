<?php
/**
 * RefundOrderResponse
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * RefundOrderResponse Class Doc Comment
 *
 * @category    Class
 * @description Refund order response
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RefundOrderResponse implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'RefundOrderResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'total_captured_amount' => 'double',
        'total_authorized_amount' => 'double',
        'refund_numbers' => 'string[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'total_captured_amount' => 'double',
        'total_authorized_amount' => 'double',
        'refund_numbers' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'total_captured_amount' => 'totalCapturedAmount',
        'total_authorized_amount' => 'totalAuthorizedAmount',
        'refund_numbers' => 'refundNumbers'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'total_captured_amount' => 'setTotalCapturedAmount',
        'total_authorized_amount' => 'setTotalAuthorizedAmount',
        'refund_numbers' => 'setRefundNumbers'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'total_captured_amount' => 'getTotalCapturedAmount',
        'total_authorized_amount' => 'getTotalAuthorizedAmount',
        'refund_numbers' => 'getRefundNumbers'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['total_captured_amount'] = isset($data['total_captured_amount']) ? $data['total_captured_amount'] : null;
        $this->container['total_authorized_amount'] = isset($data['total_authorized_amount']) ? $data['total_authorized_amount'] : null;
        $this->container['refund_numbers'] = isset($data['refund_numbers']) ? $data['refund_numbers'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets total_captured_amount
     * @return double
     */
    public function getTotalCapturedAmount()
    {
        return $this->container['total_captured_amount'];
    }

    /**
     * Sets total_captured_amount
     * @param double $total_captured_amount The total captured amount done with either a full capture, partial capture, or multiple partial captures
     * @return $this
     */
    public function setTotalCapturedAmount($total_captured_amount)
    {
        $this->container['total_captured_amount'] = $total_captured_amount;

        return $this;
    }

    /**
     * Gets total_authorized_amount
     * @return double
     */
    public function getTotalAuthorizedAmount()
    {
        return $this->container['total_authorized_amount'];
    }

    /**
     * Sets total_authorized_amount
     * @param double $total_authorized_amount Total authorized amount
     * @return $this
     */
    public function setTotalAuthorizedAmount($total_authorized_amount)
    {
        $this->container['total_authorized_amount'] = $total_authorized_amount;

        return $this;
    }

    /**
     * Gets refund_numbers
     * @return string[]
     */
    public function getRefundNumbers()
    {
        return $this->container['refund_numbers'];
    }

    /**
     * Sets refund_numbers
     * @param string[] $refund_numbers RefundNumber is assigned to an invoice that has been refunded. For one Authorization you can have multiple invoices and refundNumbers
     * @return $this
     */
    public function setRefundNumbers($refund_numbers)
    {
        $this->container['refund_numbers'] = $refund_numbers;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


