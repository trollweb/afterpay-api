<?php
/**
 * Cancellations
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * Cancellations Class Doc Comment
 *
 * @category    Class
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class Cancellations implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'Cancellations';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cancellation_no' => 'string',
        'cancellation_amount' => 'double',
        'cancellation_items' => '\Trollweb\AfterPayApi\Model\CancellationItem[]',
        'parent_transaction_reference' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cancellation_no' => null,
        'cancellation_amount' => 'double',
        'cancellation_items' => null,
        'parent_transaction_reference' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'cancellation_no' => 'cancellationNo',
        'cancellation_amount' => 'cancellationAmount',
        'cancellation_items' => 'cancellationItems',
        'parent_transaction_reference' => 'parentTransactionReference'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'cancellation_no' => 'setCancellationNo',
        'cancellation_amount' => 'setCancellationAmount',
        'cancellation_items' => 'setCancellationItems',
        'parent_transaction_reference' => 'setParentTransactionReference'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'cancellation_no' => 'getCancellationNo',
        'cancellation_amount' => 'getCancellationAmount',
        'cancellation_items' => 'getCancellationItems',
        'parent_transaction_reference' => 'getParentTransactionReference'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cancellation_no'] = isset($data['cancellation_no']) ? $data['cancellation_no'] : null;
        $this->container['cancellation_amount'] = isset($data['cancellation_amount']) ? $data['cancellation_amount'] : null;
        $this->container['cancellation_items'] = isset($data['cancellation_items']) ? $data['cancellation_items'] : null;
        $this->container['parent_transaction_reference'] = isset($data['parent_transaction_reference']) ? $data['parent_transaction_reference'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['cancellation_no']) && (strlen($this->container['cancellation_no']) > 50)) {
            $invalid_properties[] = "invalid value for 'cancellation_no', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['parent_transaction_reference']) && (strlen($this->container['parent_transaction_reference']) > 50)) {
            $invalid_properties[] = "invalid value for 'parent_transaction_reference', the character length must be smaller than or equal to 50.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['cancellation_no']) > 50) {
            return false;
        }
        if (strlen($this->container['parent_transaction_reference']) > 50) {
            return false;
        }
        return true;
    }


    /**
     * Gets cancellation_no
     * @return string
     */
    public function getCancellationNo()
    {
        return $this->container['cancellation_no'];
    }

    /**
     * Sets cancellation_no
     * @param string $cancellation_no Order number
     * @return $this
     */
    public function setCancellationNo($cancellation_no)
    {
        if (!is_null($cancellation_no) && (strlen($cancellation_no) > 50)) {
            throw new \InvalidArgumentException('invalid length for $cancellation_no when calling Cancellations., must be smaller than or equal to 50.');
        }

        $this->container['cancellation_no'] = $cancellation_no;

        return $this;
    }

    /**
     * Gets cancellation_amount
     * @return double
     */
    public function getCancellationAmount()
    {
        return $this->container['cancellation_amount'];
    }

    /**
     * Sets cancellation_amount
     * @param double $cancellation_amount Amount of the cancelled items
     * @return $this
     */
    public function setCancellationAmount($cancellation_amount)
    {
        $this->container['cancellation_amount'] = $cancellation_amount;

        return $this;
    }

    /**
     * Gets cancellation_items
     * @return \Trollweb\AfterPayApi\Model\CancellationItem[]
     */
    public function getCancellationItems()
    {
        return $this->container['cancellation_items'];
    }

    /**
     * Sets cancellation_items
     * @param \Trollweb\AfterPayApi\Model\CancellationItem[] $cancellation_items The list of items to be cancelled
     * @return $this
     */
    public function setCancellationItems($cancellation_items)
    {
        $this->container['cancellation_items'] = $cancellation_items;

        return $this;
    }

    /**
     * Gets parent_transaction_reference
     * @return string
     */
    public function getParentTransactionReference()
    {
        return $this->container['parent_transaction_reference'];
    }

    /**
     * Sets parent_transaction_reference
     * @param string $parent_transaction_reference Reference submitted by PSP
     * @return $this
     */
    public function setParentTransactionReference($parent_transaction_reference)
    {
        if (!is_null($parent_transaction_reference) && (strlen($parent_transaction_reference) > 50)) {
            throw new \InvalidArgumentException('invalid length for $parent_transaction_reference when calling Cancellations., must be smaller than or equal to 50.');
        }

        $this->container['parent_transaction_reference'] = $parent_transaction_reference;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


