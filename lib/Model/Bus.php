<?php
/**
 * Bus
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * Bus Class Doc Comment
 *
 * @category    Class
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class Bus implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'Bus';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'passengers' => '\Trollweb\AfterPayApi\Model\Passenger[]',
        'itineraries' => '\Trollweb\AfterPayApi\Model\Itinerary[]',
        'insurance' => '\Trollweb\AfterPayApi\Model\Insurance',
        'booking_reference' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'passengers' => null,
        'itineraries' => null,
        'insurance' => null,
        'booking_reference' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'passengers' => 'passengers',
        'itineraries' => 'itineraries',
        'insurance' => 'insurance',
        'booking_reference' => 'bookingReference'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'passengers' => 'setPassengers',
        'itineraries' => 'setItineraries',
        'insurance' => 'setInsurance',
        'booking_reference' => 'setBookingReference'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'passengers' => 'getPassengers',
        'itineraries' => 'getItineraries',
        'insurance' => 'getInsurance',
        'booking_reference' => 'getBookingReference'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['passengers'] = isset($data['passengers']) ? $data['passengers'] : null;
        $this->container['itineraries'] = isset($data['itineraries']) ? $data['itineraries'] : null;
        $this->container['insurance'] = isset($data['insurance']) ? $data['insurance'] : null;
        $this->container['booking_reference'] = isset($data['booking_reference']) ? $data['booking_reference'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['booking_reference']) && (strlen($this->container['booking_reference']) > 4096)) {
            $invalid_properties[] = "invalid value for 'booking_reference', the character length must be smaller than or equal to 4096.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['booking_reference']) > 4096) {
            return false;
        }
        return true;
    }


    /**
     * Gets passengers
     * @return \Trollweb\AfterPayApi\Model\Passenger[]
     */
    public function getPassengers()
    {
        return $this->container['passengers'];
    }

    /**
     * Sets passengers
     * @param \Trollweb\AfterPayApi\Model\Passenger[] $passengers Passengers
     * @return $this
     */
    public function setPassengers($passengers)
    {
        $this->container['passengers'] = $passengers;

        return $this;
    }

    /**
     * Gets itineraries
     * @return \Trollweb\AfterPayApi\Model\Itinerary[]
     */
    public function getItineraries()
    {
        return $this->container['itineraries'];
    }

    /**
     * Sets itineraries
     * @param \Trollweb\AfterPayApi\Model\Itinerary[] $itineraries Itineraties
     * @return $this
     */
    public function setItineraries($itineraries)
    {
        $this->container['itineraries'] = $itineraries;

        return $this;
    }

    /**
     * Gets insurance
     * @return \Trollweb\AfterPayApi\Model\Insurance
     */
    public function getInsurance()
    {
        return $this->container['insurance'];
    }

    /**
     * Sets insurance
     * @param \Trollweb\AfterPayApi\Model\Insurance $insurance Insurance
     * @return $this
     */
    public function setInsurance($insurance)
    {
        $this->container['insurance'] = $insurance;

        return $this;
    }

    /**
     * Gets booking_reference
     * @return string
     */
    public function getBookingReference()
    {
        return $this->container['booking_reference'];
    }

    /**
     * Sets booking_reference
     * @param string $booking_reference Booking reference
     * @return $this
     */
    public function setBookingReference($booking_reference)
    {
        if (!is_null($booking_reference) && (strlen($booking_reference) > 4096)) {
            throw new \InvalidArgumentException('invalid length for $booking_reference when calling Bus., must be smaller than or equal to 4096.');
        }

        $this->container['booking_reference'] = $booking_reference;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


