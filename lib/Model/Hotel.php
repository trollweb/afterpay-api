<?php
/**
 * Hotel
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * Hotel Class Doc Comment
 *
 * @category    Class
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class Hotel implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'Hotel';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'company' => 'string',
        'address' => '\Trollweb\AfterPayApi\Model\Address',
        'checkin' => '\DateTime',
        'checkout' => '\DateTime',
        'guests' => '\Trollweb\AfterPayApi\Model\CheckoutCustomer[]',
        'number_of_rooms' => 'int',
        'price' => 'double',
        'currency' => 'string',
        'booking_reference' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'company' => null,
        'address' => null,
        'checkin' => 'date-time',
        'checkout' => 'date-time',
        'guests' => null,
        'number_of_rooms' => 'int32',
        'price' => 'double',
        'currency' => null,
        'booking_reference' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'company' => 'company',
        'address' => 'address',
        'checkin' => 'checkin',
        'checkout' => 'checkout',
        'guests' => 'guests',
        'number_of_rooms' => 'numberOfRooms',
        'price' => 'price',
        'currency' => 'currency',
        'booking_reference' => 'bookingReference'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'company' => 'setCompany',
        'address' => 'setAddress',
        'checkin' => 'setCheckin',
        'checkout' => 'setCheckout',
        'guests' => 'setGuests',
        'number_of_rooms' => 'setNumberOfRooms',
        'price' => 'setPrice',
        'currency' => 'setCurrency',
        'booking_reference' => 'setBookingReference'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'company' => 'getCompany',
        'address' => 'getAddress',
        'checkin' => 'getCheckin',
        'checkout' => 'getCheckout',
        'guests' => 'getGuests',
        'number_of_rooms' => 'getNumberOfRooms',
        'price' => 'getPrice',
        'currency' => 'getCurrency',
        'booking_reference' => 'getBookingReference'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const CURRENCY_EUR = 'EUR';
    const CURRENCY_NOK = 'NOK';
    const CURRENCY_SEK = 'SEK';
    const CURRENCY_DKK = 'DKK';
    const CURRENCY_CHF = 'CHF';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getCurrencyAllowableValues()
    {
        return [
            self::CURRENCY_EUR,
            self::CURRENCY_NOK,
            self::CURRENCY_SEK,
            self::CURRENCY_DKK,
            self::CURRENCY_CHF,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['company'] = isset($data['company']) ? $data['company'] : null;
        $this->container['address'] = isset($data['address']) ? $data['address'] : null;
        $this->container['checkin'] = isset($data['checkin']) ? $data['checkin'] : null;
        $this->container['checkout'] = isset($data['checkout']) ? $data['checkout'] : null;
        $this->container['guests'] = isset($data['guests']) ? $data['guests'] : null;
        $this->container['number_of_rooms'] = isset($data['number_of_rooms']) ? $data['number_of_rooms'] : null;
        $this->container['price'] = isset($data['price']) ? $data['price'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['booking_reference'] = isset($data['booking_reference']) ? $data['booking_reference'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['company']) && (strlen($this->container['company']) > 4096)) {
            $invalid_properties[] = "invalid value for 'company', the character length must be smaller than or equal to 4096.";
        }

        $allowed_values = $this->getCurrencyAllowableValues();
        if (!in_array($this->container['currency'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'currency', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        if (!is_null($this->container['booking_reference']) && (strlen($this->container['booking_reference']) > 4096)) {
            $invalid_properties[] = "invalid value for 'booking_reference', the character length must be smaller than or equal to 4096.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['company']) > 4096) {
            return false;
        }
        $allowed_values = $this->getCurrencyAllowableValues();
        if (!in_array($this->container['currency'], $allowed_values)) {
            return false;
        }
        if (strlen($this->container['booking_reference']) > 4096) {
            return false;
        }
        return true;
    }


    /**
     * Gets company
     * @return string
     */
    public function getCompany()
    {
        return $this->container['company'];
    }

    /**
     * Sets company
     * @param string $company
     * @return $this
     */
    public function setCompany($company)
    {
        if (!is_null($company) && (strlen($company) > 4096)) {
            throw new \InvalidArgumentException('invalid length for $company when calling Hotel., must be smaller than or equal to 4096.');
        }

        $this->container['company'] = $company;

        return $this;
    }

    /**
     * Gets address
     * @return \Trollweb\AfterPayApi\Model\Address
     */
    public function getAddress()
    {
        return $this->container['address'];
    }

    /**
     * Sets address
     * @param \Trollweb\AfterPayApi\Model\Address $address Hotel address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->container['address'] = $address;

        return $this;
    }

    /**
     * Gets checkin
     * @return \DateTime
     */
    public function getCheckin()
    {
        return $this->container['checkin'];
    }

    /**
     * Sets checkin
     * @param \DateTime $checkin Check-in date and time
     * @return $this
     */
    public function setCheckin($checkin)
    {
        $this->container['checkin'] = $checkin;

        return $this;
    }

    /**
     * Gets checkout
     * @return \DateTime
     */
    public function getCheckout()
    {
        return $this->container['checkout'];
    }

    /**
     * Sets checkout
     * @param \DateTime $checkout Check-out date and time
     * @return $this
     */
    public function setCheckout($checkout)
    {
        $this->container['checkout'] = $checkout;

        return $this;
    }

    /**
     * Gets guests
     * @return \Trollweb\AfterPayApi\Model\CheckoutCustomer[]
     */
    public function getGuests()
    {
        return $this->container['guests'];
    }

    /**
     * Sets guests
     * @param \Trollweb\AfterPayApi\Model\CheckoutCustomer[] $guests Guests information
     * @return $this
     */
    public function setGuests($guests)
    {
        $this->container['guests'] = $guests;

        return $this;
    }

    /**
     * Gets number_of_rooms
     * @return int
     */
    public function getNumberOfRooms()
    {
        return $this->container['number_of_rooms'];
    }

    /**
     * Sets number_of_rooms
     * @param int $number_of_rooms Number of rooms
     * @return $this
     */
    public function setNumberOfRooms($number_of_rooms)
    {
        $this->container['number_of_rooms'] = $number_of_rooms;

        return $this;
    }

    /**
     * Gets price
     * @return double
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     * @param double $price Price of the hotel accommodation
     * @return $this
     */
    public function setPrice($price)
    {
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     * @param string $currency Currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $allowed_values = $this->getCurrencyAllowableValues();
        if (!is_null($currency) && !in_array($currency, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'currency', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets booking_reference
     * @return string
     */
    public function getBookingReference()
    {
        return $this->container['booking_reference'];
    }

    /**
     * Sets booking_reference
     * @param string $booking_reference Booking reference
     * @return $this
     */
    public function setBookingReference($booking_reference)
    {
        if (!is_null($booking_reference) && (strlen($booking_reference) > 4096)) {
            throw new \InvalidArgumentException('invalid length for $booking_reference when calling Hotel., must be smaller than or equal to 4096.');
        }

        $this->container['booking_reference'] = $booking_reference;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


