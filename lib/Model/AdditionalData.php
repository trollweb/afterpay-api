<?php
/**
 * AdditionalData
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * AdditionalData Class Doc Comment
 *
 * @category    Class
 * @description Additional data
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AdditionalData implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'AdditionalData';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'plugin_provider' => 'string',
        'plugin_version' => 'string',
        'shop_url' => 'string',
        'shop_platform' => 'string',
        'shop_platform_version' => 'string',
        'marketplace' => '\Trollweb\AfterPayApi\Model\Marketplace[]',
        'subscription' => '\Trollweb\AfterPayApi\Model\Subscription',
        'partner_data' => '\Trollweb\AfterPayApi\Model\PartnerData'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'plugin_provider' => null,
        'plugin_version' => null,
        'shop_url' => null,
        'shop_platform' => null,
        'shop_platform_version' => null,
        'marketplace' => null,
        'subscription' => null,
        'partner_data' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'plugin_provider' => 'pluginProvider',
        'plugin_version' => 'pluginVersion',
        'shop_url' => 'shopUrl',
        'shop_platform' => 'shopPlatform',
        'shop_platform_version' => 'shopPlatformVersion',
        'marketplace' => 'marketplace',
        'subscription' => 'subscription',
        'partner_data' => 'partnerData'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'plugin_provider' => 'setPluginProvider',
        'plugin_version' => 'setPluginVersion',
        'shop_url' => 'setShopUrl',
        'shop_platform' => 'setShopPlatform',
        'shop_platform_version' => 'setShopPlatformVersion',
        'marketplace' => 'setMarketplace',
        'subscription' => 'setSubscription',
        'partner_data' => 'setPartnerData'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'plugin_provider' => 'getPluginProvider',
        'plugin_version' => 'getPluginVersion',
        'shop_url' => 'getShopUrl',
        'shop_platform' => 'getShopPlatform',
        'shop_platform_version' => 'getShopPlatformVersion',
        'marketplace' => 'getMarketplace',
        'subscription' => 'getSubscription',
        'partner_data' => 'getPartnerData'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['plugin_provider'] = isset($data['plugin_provider']) ? $data['plugin_provider'] : null;
        $this->container['plugin_version'] = isset($data['plugin_version']) ? $data['plugin_version'] : null;
        $this->container['shop_url'] = isset($data['shop_url']) ? $data['shop_url'] : null;
        $this->container['shop_platform'] = isset($data['shop_platform']) ? $data['shop_platform'] : null;
        $this->container['shop_platform_version'] = isset($data['shop_platform_version']) ? $data['shop_platform_version'] : null;
        $this->container['marketplace'] = isset($data['marketplace']) ? $data['marketplace'] : null;
        $this->container['subscription'] = isset($data['subscription']) ? $data['subscription'] : null;
        $this->container['partner_data'] = isset($data['partner_data']) ? $data['partner_data'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['plugin_provider']) && (strlen($this->container['plugin_provider']) > 50)) {
            $invalid_properties[] = "invalid value for 'plugin_provider', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['plugin_version']) && (strlen($this->container['plugin_version']) > 50)) {
            $invalid_properties[] = "invalid value for 'plugin_version', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['shop_url']) && (strlen($this->container['shop_url']) > 2048)) {
            $invalid_properties[] = "invalid value for 'shop_url', the character length must be smaller than or equal to 2048.";
        }

        if (!is_null($this->container['shop_platform']) && (strlen($this->container['shop_platform']) > 50)) {
            $invalid_properties[] = "invalid value for 'shop_platform', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['shop_platform_version']) && (strlen($this->container['shop_platform_version']) > 50)) {
            $invalid_properties[] = "invalid value for 'shop_platform_version', the character length must be smaller than or equal to 50.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['plugin_provider']) > 50) {
            return false;
        }
        if (strlen($this->container['plugin_version']) > 50) {
            return false;
        }
        if (strlen($this->container['shop_url']) > 2048) {
            return false;
        }
        if (strlen($this->container['shop_platform']) > 50) {
            return false;
        }
        if (strlen($this->container['shop_platform_version']) > 50) {
            return false;
        }
        return true;
    }


    /**
     * Gets plugin_provider
     * @return string
     */
    public function getPluginProvider()
    {
        return $this->container['plugin_provider'];
    }

    /**
     * Sets plugin_provider
     * @param string $plugin_provider Plugin Provider Name
     * @return $this
     */
    public function setPluginProvider($plugin_provider)
    {
        if (!is_null($plugin_provider) && (strlen($plugin_provider) > 50)) {
            throw new \InvalidArgumentException('invalid length for $plugin_provider when calling AdditionalData., must be smaller than or equal to 50.');
        }

        $this->container['plugin_provider'] = $plugin_provider;

        return $this;
    }

    /**
     * Gets plugin_version
     * @return string
     */
    public function getPluginVersion()
    {
        return $this->container['plugin_version'];
    }

    /**
     * Sets plugin_version
     * @param string $plugin_version Plugin Version
     * @return $this
     */
    public function setPluginVersion($plugin_version)
    {
        if (!is_null($plugin_version) && (strlen($plugin_version) > 50)) {
            throw new \InvalidArgumentException('invalid length for $plugin_version when calling AdditionalData., must be smaller than or equal to 50.');
        }

        $this->container['plugin_version'] = $plugin_version;

        return $this;
    }

    /**
     * Gets shop_url
     * @return string
     */
    public function getShopUrl()
    {
        return $this->container['shop_url'];
    }

    /**
     * Sets shop_url
     * @param string $shop_url URL of the Webshop
     * @return $this
     */
    public function setShopUrl($shop_url)
    {
        if (!is_null($shop_url) && (strlen($shop_url) > 2048)) {
            throw new \InvalidArgumentException('invalid length for $shop_url when calling AdditionalData., must be smaller than or equal to 2048.');
        }

        $this->container['shop_url'] = $shop_url;

        return $this;
    }

    /**
     * Gets shop_platform
     * @return string
     */
    public function getShopPlatform()
    {
        return $this->container['shop_platform'];
    }

    /**
     * Sets shop_platform
     * @param string $shop_platform Name of the platform used for the Webshop
     * @return $this
     */
    public function setShopPlatform($shop_platform)
    {
        if (!is_null($shop_platform) && (strlen($shop_platform) > 50)) {
            throw new \InvalidArgumentException('invalid length for $shop_platform when calling AdditionalData., must be smaller than or equal to 50.');
        }

        $this->container['shop_platform'] = $shop_platform;

        return $this;
    }

    /**
     * Gets shop_platform_version
     * @return string
     */
    public function getShopPlatformVersion()
    {
        return $this->container['shop_platform_version'];
    }

    /**
     * Sets shop_platform_version
     * @param string $shop_platform_version Version of the webshop platform
     * @return $this
     */
    public function setShopPlatformVersion($shop_platform_version)
    {
        if (!is_null($shop_platform_version) && (strlen($shop_platform_version) > 50)) {
            throw new \InvalidArgumentException('invalid length for $shop_platform_version when calling AdditionalData., must be smaller than or equal to 50.');
        }

        $this->container['shop_platform_version'] = $shop_platform_version;

        return $this;
    }

    /**
     * Gets marketplace
     * @return \Trollweb\AfterPayApi\Model\Marketplace[]
     */
    public function getMarketplace()
    {
        return $this->container['marketplace'];
    }

    /**
     * Sets marketplace
     * @param \Trollweb\AfterPayApi\Model\Marketplace[] $marketplace Additional information for marketplace set-ups
     * @return $this
     */
    public function setMarketplace($marketplace)
    {
        $this->container['marketplace'] = $marketplace;

        return $this;
    }

    /**
     * Gets subscription
     * @return \Trollweb\AfterPayApi\Model\Subscription
     */
    public function getSubscription()
    {
        return $this->container['subscription'];
    }

    /**
     * Sets subscription
     * @param \Trollweb\AfterPayApi\Model\Subscription $subscription Additional information for subscription businesses
     * @return $this
     */
    public function setSubscription($subscription)
    {
        $this->container['subscription'] = $subscription;

        return $this;
    }

    /**
     * Gets partner_data
     * @return \Trollweb\AfterPayApi\Model\PartnerData
     */
    public function getPartnerData()
    {
        return $this->container['partner_data'];
    }

    /**
     * Sets partner_data
     * @param \Trollweb\AfterPayApi\Model\PartnerData $partner_data Additional data to be provided by PSP or platform provider
     * @return $this
     */
    public function setPartnerData($partner_data)
    {
        $this->container['partner_data'] = $partner_data;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


