<?php
/**
 * Marketplace
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * Marketplace Class Doc Comment
 *
 * @category    Class
 * @description Marketplace
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class Marketplace implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'Marketplace';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'string',
        'name' => 'string',
        'registered_since' => '\DateTime',
        'rating' => 'string',
        'transactions' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => null,
        'name' => null,
        'registered_since' => 'date-time',
        'rating' => null,
        'transactions' => 'int32'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'name' => 'name',
        'registered_since' => 'registeredSince',
        'rating' => 'rating',
        'transactions' => 'transactions'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'name' => 'setName',
        'registered_since' => 'setRegisteredSince',
        'rating' => 'setRating',
        'transactions' => 'setTransactions'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'name' => 'getName',
        'registered_since' => 'getRegisteredSince',
        'rating' => 'getRating',
        'transactions' => 'getTransactions'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['registered_since'] = isset($data['registered_since']) ? $data['registered_since'] : null;
        $this->container['rating'] = isset($data['rating']) ? $data['rating'] : null;
        $this->container['transactions'] = isset($data['transactions']) ? $data['transactions'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['id']) && (strlen($this->container['id']) > 32)) {
            $invalid_properties[] = "invalid value for 'id', the character length must be smaller than or equal to 32.";
        }

        if (!is_null($this->container['name']) && (strlen($this->container['name']) > 50)) {
            $invalid_properties[] = "invalid value for 'name', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['rating']) && (strlen($this->container['rating']) > 20)) {
            $invalid_properties[] = "invalid value for 'rating', the character length must be smaller than or equal to 20.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['id']) > 32) {
            return false;
        }
        if (strlen($this->container['name']) > 50) {
            return false;
        }
        if (strlen($this->container['rating']) > 20) {
            return false;
        }
        return true;
    }


    /**
     * Gets id
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param string $id ID of the marketplace seller
     * @return $this
     */
    public function setId($id)
    {
        if (!is_null($id) && (strlen($id) > 32)) {
            throw new \InvalidArgumentException('invalid length for $id when calling Marketplace., must be smaller than or equal to 32.');
        }

        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets name
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     * @param string $name Name of the marketplace seller
     * @return $this
     */
    public function setName($name)
    {
        if (!is_null($name) && (strlen($name) > 50)) {
            throw new \InvalidArgumentException('invalid length for $name when calling Marketplace., must be smaller than or equal to 50.');
        }

        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets registered_since
     * @return \DateTime
     */
    public function getRegisteredSince()
    {
        return $this->container['registered_since'];
    }

    /**
     * Sets registered_since
     * @param \DateTime $registered_since Registration date of the marketplace seller
     * @return $this
     */
    public function setRegisteredSince($registered_since)
    {
        $this->container['registered_since'] = $registered_since;

        return $this;
    }

    /**
     * Gets rating
     * @return string
     */
    public function getRating()
    {
        return $this->container['rating'];
    }

    /**
     * Sets rating
     * @param string $rating Rating of the marketplace seller
     * @return $this
     */
    public function setRating($rating)
    {
        if (!is_null($rating) && (strlen($rating) > 20)) {
            throw new \InvalidArgumentException('invalid length for $rating when calling Marketplace., must be smaller than or equal to 20.');
        }

        $this->container['rating'] = $rating;

        return $this;
    }

    /**
     * Gets transactions
     * @return int
     */
    public function getTransactions()
    {
        return $this->container['transactions'];
    }

    /**
     * Sets transactions
     * @param int $transactions Number of transaction processed by the marketplace seller
     * @return $this
     */
    public function setTransactions($transactions)
    {
        $this->container['transactions'] = $transactions;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


