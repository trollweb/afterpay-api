<?php
/**
 * ValidateBankAccountRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * ValidateBankAccountRequest Class Doc Comment
 *
 * @category    Class
 * @description Check account request
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ValidateBankAccountRequest implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ValidateBankAccountRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'bank_account' => 'string',
        'bank_code' => 'string',
        'bank_number' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'bank_account' => null,
        'bank_code' => null,
        'bank_number' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'bank_account' => 'bankAccount',
        'bank_code' => 'bankCode',
        'bank_number' => 'bankNumber'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'bank_account' => 'setBankAccount',
        'bank_code' => 'setBankCode',
        'bank_number' => 'setBankNumber'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'bank_account' => 'getBankAccount',
        'bank_code' => 'getBankCode',
        'bank_number' => 'getBankNumber'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['bank_account'] = isset($data['bank_account']) ? $data['bank_account'] : null;
        $this->container['bank_code'] = isset($data['bank_code']) ? $data['bank_code'] : null;
        $this->container['bank_number'] = isset($data['bank_number']) ? $data['bank_number'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['bank_account'] === null) {
            $invalid_properties[] = "'bank_account' can't be null";
        }
        if ((strlen($this->container['bank_account']) > 34)) {
            $invalid_properties[] = "invalid value for 'bank_account', the character length must be smaller than or equal to 34.";
        }

        if (!is_null($this->container['bank_code']) && (strlen($this->container['bank_code']) > 11)) {
            $invalid_properties[] = "invalid value for 'bank_code', the character length must be smaller than or equal to 11.";
        }

        if (!is_null($this->container['bank_number']) && (strlen($this->container['bank_number']) > 50)) {
            $invalid_properties[] = "invalid value for 'bank_number', the character length must be smaller than or equal to 50.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['bank_account'] === null) {
            return false;
        }
        if (strlen($this->container['bank_account']) > 34) {
            return false;
        }
        if (strlen($this->container['bank_code']) > 11) {
            return false;
        }
        if (strlen($this->container['bank_number']) > 50) {
            return false;
        }
        return true;
    }


    /**
     * Gets bank_account
     * @return string
     */
    public function getBankAccount()
    {
        return $this->container['bank_account'];
    }

    /**
     * Sets bank_account
     * @param string $bank_account Account number
     * @return $this
     */
    public function setBankAccount($bank_account)
    {
        if ((strlen($bank_account) > 34)) {
            throw new \InvalidArgumentException('invalid length for $bank_account when calling ValidateBankAccountRequest., must be smaller than or equal to 34.');
        }

        $this->container['bank_account'] = $bank_account;

        return $this;
    }

    /**
     * Gets bank_code
     * @return string
     */
    public function getBankCode()
    {
        return $this->container['bank_code'];
    }

    /**
     * Sets bank_code
     * @param string $bank_code Account swift number
     * @return $this
     */
    public function setBankCode($bank_code)
    {
        if (!is_null($bank_code) && (strlen($bank_code) > 11)) {
            throw new \InvalidArgumentException('invalid length for $bank_code when calling ValidateBankAccountRequest., must be smaller than or equal to 11.');
        }

        $this->container['bank_code'] = $bank_code;

        return $this;
    }

    /**
     * Gets bank_number
     * @return string
     */
    public function getBankNumber()
    {
        return $this->container['bank_number'];
    }

    /**
     * Sets bank_number
     * @param string $bank_number Bank and branch identifier
     * @return $this
     */
    public function setBankNumber($bank_number)
    {
        if (!is_null($bank_number) && (strlen($bank_number) > 50)) {
            throw new \InvalidArgumentException('invalid length for $bank_number when calling ValidateBankAccountRequest., must be smaller than or equal to 50.');
        }

        $this->container['bank_number'] = $bank_number;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


