<?php
/**
 * UserProfile
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\AfterPayApi\Model;

use \ArrayAccess;

/**
 * UserProfile Class Doc Comment
 *
 * @category    Class
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class UserProfile implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'UserProfile';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'first_name' => 'string',
        'last_name' => 'string',
        'mobile_number' => 'string',
        'e_mail' => 'string',
        'language_code' => 'string',
        'address_list' => '\Trollweb\AfterPayApi\Model\LookupAddress[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'first_name' => null,
        'last_name' => null,
        'mobile_number' => null,
        'e_mail' => null,
        'language_code' => null,
        'address_list' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'first_name' => 'firstName',
        'last_name' => 'lastName',
        'mobile_number' => 'mobileNumber',
        'e_mail' => 'eMail',
        'language_code' => 'languageCode',
        'address_list' => 'addressList'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'first_name' => 'setFirstName',
        'last_name' => 'setLastName',
        'mobile_number' => 'setMobileNumber',
        'e_mail' => 'setEMail',
        'language_code' => 'setLanguageCode',
        'address_list' => 'setAddressList'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'first_name' => 'getFirstName',
        'last_name' => 'getLastName',
        'mobile_number' => 'getMobileNumber',
        'e_mail' => 'getEMail',
        'language_code' => 'getLanguageCode',
        'address_list' => 'getAddressList'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['first_name'] = isset($data['first_name']) ? $data['first_name'] : null;
        $this->container['last_name'] = isset($data['last_name']) ? $data['last_name'] : null;
        $this->container['mobile_number'] = isset($data['mobile_number']) ? $data['mobile_number'] : null;
        $this->container['e_mail'] = isset($data['e_mail']) ? $data['e_mail'] : null;
        $this->container['language_code'] = isset($data['language_code']) ? $data['language_code'] : null;
        $this->container['address_list'] = isset($data['address_list']) ? $data['address_list'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if (!is_null($this->container['first_name']) && (strlen($this->container['first_name']) > 50)) {
            $invalid_properties[] = "invalid value for 'first_name', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['last_name']) && (strlen($this->container['last_name']) > 50)) {
            $invalid_properties[] = "invalid value for 'last_name', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['mobile_number']) && (strlen($this->container['mobile_number']) > 20)) {
            $invalid_properties[] = "invalid value for 'mobile_number', the character length must be smaller than or equal to 20.";
        }

        if (!is_null($this->container['e_mail']) && (strlen($this->container['e_mail']) > 255)) {
            $invalid_properties[] = "invalid value for 'e_mail', the character length must be smaller than or equal to 255.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if (strlen($this->container['first_name']) > 50) {
            return false;
        }
        if (strlen($this->container['last_name']) > 50) {
            return false;
        }
        if (strlen($this->container['mobile_number']) > 20) {
            return false;
        }
        if (strlen($this->container['e_mail']) > 255) {
            return false;
        }
        return true;
    }


    /**
     * Gets first_name
     * @return string
     */
    public function getFirstName()
    {
        return $this->container['first_name'];
    }

    /**
     * Sets first_name
     * @param string $first_name First name
     * @return $this
     */
    public function setFirstName($first_name)
    {
        if (!is_null($first_name) && (strlen($first_name) > 50)) {
            throw new \InvalidArgumentException('invalid length for $first_name when calling UserProfile., must be smaller than or equal to 50.');
        }

        $this->container['first_name'] = $first_name;

        return $this;
    }

    /**
     * Gets last_name
     * @return string
     */
    public function getLastName()
    {
        return $this->container['last_name'];
    }

    /**
     * Sets last_name
     * @param string $last_name Last name
     * @return $this
     */
    public function setLastName($last_name)
    {
        if (!is_null($last_name) && (strlen($last_name) > 50)) {
            throw new \InvalidArgumentException('invalid length for $last_name when calling UserProfile., must be smaller than or equal to 50.');
        }

        $this->container['last_name'] = $last_name;

        return $this;
    }

    /**
     * Gets mobile_number
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->container['mobile_number'];
    }

    /**
     * Sets mobile_number
     * @param string $mobile_number Mobile number
     * @return $this
     */
    public function setMobileNumber($mobile_number)
    {
        if (!is_null($mobile_number) && (strlen($mobile_number) > 20)) {
            throw new \InvalidArgumentException('invalid length for $mobile_number when calling UserProfile., must be smaller than or equal to 20.');
        }

        $this->container['mobile_number'] = $mobile_number;

        return $this;
    }

    /**
     * Gets e_mail
     * @return string
     */
    public function getEMail()
    {
        return $this->container['e_mail'];
    }

    /**
     * Sets e_mail
     * @param string $e_mail Email address
     * @return $this
     */
    public function setEMail($e_mail)
    {
        if (!is_null($e_mail) && (strlen($e_mail) > 255)) {
            throw new \InvalidArgumentException('invalid length for $e_mail when calling UserProfile., must be smaller than or equal to 255.');
        }

        $this->container['e_mail'] = $e_mail;

        return $this;
    }

    /**
     * Gets language_code
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->container['language_code'];
    }

    /**
     * Sets language_code
     * @param string $language_code Language code
     * @return $this
     */
    public function setLanguageCode($language_code)
    {
        $this->container['language_code'] = $language_code;

        return $this;
    }

    /**
     * Gets address_list
     * @return \Trollweb\AfterPayApi\Model\LookupAddress[]
     */
    public function getAddressList()
    {
        return $this->container['address_list'];
    }

    /**
     * Sets address_list
     * @param \Trollweb\AfterPayApi\Model\LookupAddress[] $address_list Array of LookupAddress objects
     * @return $this
     */
    public function setAddressList($address_list)
    {
        $this->container['address_list'] = $address_list;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\AfterPayApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


