<?php
/**
 * OrderSummaryTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\AfterPayApi;

/**
 * OrderSummaryTest Class Doc Comment
 *
 * @category    Class */
// * @description Order details
/**
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderSummaryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "OrderSummary"
     */
    public function testOrderSummary()
    {
    }

    /**
     * Test attribute "total_net_amount"
     */
    public function testPropertyTotalNetAmount()
    {
    }

    /**
     * Test attribute "total_gross_amount"
     */
    public function testPropertyTotalGrossAmount()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }

    /**
     * Test attribute "risk"
     */
    public function testPropertyRisk()
    {
    }

    /**
     * Test attribute "merchant_image_url"
     */
    public function testPropertyMerchantImageUrl()
    {
    }

    /**
     * Test attribute "image_url"
     */
    public function testPropertyImageUrl()
    {
    }

    /**
     * Test attribute "google_analytics_user_id"
     */
    public function testPropertyGoogleAnalyticsUserId()
    {
    }

    /**
     * Test attribute "google_analytics_client_id"
     */
    public function testPropertyGoogleAnalyticsClientId()
    {
    }

    /**
     * Test attribute "items"
     */
    public function testPropertyItems()
    {
    }
}
