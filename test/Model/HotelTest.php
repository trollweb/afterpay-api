<?php
/**
 * HotelTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\AfterPayApi;

/**
 * HotelTest Class Doc Comment
 *
 * @category    Class */
// * @description Hotel
/**
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class HotelTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Hotel"
     */
    public function testHotel()
    {
    }

    /**
     * Test attribute "company"
     */
    public function testPropertyCompany()
    {
    }

    /**
     * Test attribute "address"
     */
    public function testPropertyAddress()
    {
    }

    /**
     * Test attribute "checkin"
     */
    public function testPropertyCheckin()
    {
    }

    /**
     * Test attribute "checkout"
     */
    public function testPropertyCheckout()
    {
    }

    /**
     * Test attribute "guests"
     */
    public function testPropertyGuests()
    {
    }

    /**
     * Test attribute "number_of_rooms"
     */
    public function testPropertyNumberOfRooms()
    {
    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }

    /**
     * Test attribute "booking_reference"
     */
    public function testPropertyBookingReference()
    {
    }
}
