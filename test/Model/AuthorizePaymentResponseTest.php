<?php
/**
 * AuthorizePaymentResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\AfterPayApi;

/**
 * AuthorizePaymentResponseTest Class Doc Comment
 *
 * @category    Class */
// * @description Authorize Payment Response
/**
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AuthorizePaymentResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AuthorizePaymentResponse"
     */
    public function testAuthorizePaymentResponse()
    {
    }

    /**
     * Test attribute "outcome"
     */
    public function testPropertyOutcome()
    {
    }

    /**
     * Test attribute "customer"
     */
    public function testPropertyCustomer()
    {
    }

    /**
     * Test attribute "delivery_customer"
     */
    public function testPropertyDeliveryCustomer()
    {
    }

    /**
     * Test attribute "reservation_id"
     */
    public function testPropertyReservationId()
    {
    }

    /**
     * Test attribute "checkout_id"
     */
    public function testPropertyCheckoutId()
    {
    }

    /**
     * Test attribute "risk_check_messages"
     */
    public function testPropertyRiskCheckMessages()
    {
    }
}
