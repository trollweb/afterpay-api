<?php
/**
 * AddressTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\AfterPayApi;

/**
 * AddressTest Class Doc Comment
 *
 * @category    Class */
// * @description Customer address
/**
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AddressTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Address"
     */
    public function testAddress()
    {
    }

    /**
     * Test attribute "street"
     */
    public function testPropertyStreet()
    {
    }

    /**
     * Test attribute "street_number"
     */
    public function testPropertyStreetNumber()
    {
    }

    /**
     * Test attribute "street_number_additional"
     */
    public function testPropertyStreetNumberAdditional()
    {
    }

    /**
     * Test attribute "postal_code"
     */
    public function testPropertyPostalCode()
    {
    }

    /**
     * Test attribute "postal_place"
     */
    public function testPropertyPostalPlace()
    {
    }

    /**
     * Test attribute "country_code"
     */
    public function testPropertyCountryCode()
    {
    }

    /**
     * Test attribute "address_type"
     */
    public function testPropertyAddressType()
    {
    }

    /**
     * Test attribute "care_of"
     */
    public function testPropertyCareOf()
    {
    }
}
