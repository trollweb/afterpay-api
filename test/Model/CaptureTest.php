<?php
/**
 * CaptureTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\AfterPayApi;

/**
 * CaptureTest Class Doc Comment
 *
 * @category    Class */
// * @description Capture
/**
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CaptureTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Capture"
     */
    public function testCapture()
    {
    }

    /**
     * Test attribute "capture_id"
     */
    public function testPropertyCaptureId()
    {
    }

    /**
     * Test attribute "reservation_id"
     */
    public function testPropertyReservationId()
    {
    }

    /**
     * Test attribute "customer_number"
     */
    public function testPropertyCustomerNumber()
    {
    }

    /**
     * Test attribute "capture_number"
     */
    public function testPropertyCaptureNumber()
    {
    }

    /**
     * Test attribute "order_number"
     */
    public function testPropertyOrderNumber()
    {
    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {
    }

    /**
     * Test attribute "balance"
     */
    public function testPropertyBalance()
    {
    }

    /**
     * Test attribute "total_refunded_amount"
     */
    public function testPropertyTotalRefundedAmount()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }

    /**
     * Test attribute "inserted_at"
     */
    public function testPropertyInsertedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }

    /**
     * Test attribute "direct_debit_bank_account"
     */
    public function testPropertyDirectDebitBankAccount()
    {
    }

    /**
     * Test attribute "direct_debit_swift"
     */
    public function testPropertyDirectDebitSwift()
    {
    }

    /**
     * Test attribute "account_profile_number"
     */
    public function testPropertyAccountProfileNumber()
    {
    }

    /**
     * Test attribute "number_of_installments"
     */
    public function testPropertyNumberOfInstallments()
    {
    }

    /**
     * Test attribute "installment_amount"
     */
    public function testPropertyInstallmentAmount()
    {
    }

    /**
     * Test attribute "contract_date"
     */
    public function testPropertyContractDate()
    {
    }

    /**
     * Test attribute "order_date"
     */
    public function testPropertyOrderDate()
    {
    }

    /**
     * Test attribute "installment_profile_number"
     */
    public function testPropertyInstallmentProfileNumber()
    {
    }

    /**
     * Test attribute "parent_transaction_reference"
     */
    public function testPropertyParentTransactionReference()
    {
    }

    /**
     * Test attribute "due_date"
     */
    public function testPropertyDueDate()
    {
    }

    /**
     * Test attribute "invoice_date"
     */
    public function testPropertyInvoiceDate()
    {
    }

    /**
     * Test attribute "your_reference"
     */
    public function testPropertyYourReference()
    {
    }

    /**
     * Test attribute "our_reference"
     */
    public function testPropertyOurReference()
    {
    }

    /**
     * Test attribute "invoice_profile_number"
     */
    public function testPropertyInvoiceProfileNumber()
    {
    }

    /**
     * Test attribute "ocr"
     */
    public function testPropertyOcr()
    {
    }

    /**
     * Test attribute "installment_customer_interest_rate"
     */
    public function testPropertyInstallmentCustomerInterestRate()
    {
    }

    /**
     * Test attribute "image_url"
     */
    public function testPropertyImageUrl()
    {
    }

    /**
     * Test attribute "capture_items"
     */
    public function testPropertyCaptureItems()
    {
    }

    /**
     * Test attribute "shipping_details"
     */
    public function testPropertyShippingDetails()
    {
    }
}
