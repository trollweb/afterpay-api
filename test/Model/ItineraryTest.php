<?php
/**
 * ItineraryTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\AfterPayApi;

/**
 * ItineraryTest Class Doc Comment
 *
 * @category    Class */
// * @description Itinerary
/**
 * @package     Trollweb\AfterPayApi
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ItineraryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Itinerary"
     */
    public function testItinerary()
    {
    }

    /**
     * Test attribute "operator"
     */
    public function testPropertyOperator()
    {
    }

    /**
     * Test attribute "departure"
     */
    public function testPropertyDeparture()
    {
    }

    /**
     * Test attribute "arrival"
     */
    public function testPropertyArrival()
    {
    }

    /**
     * Test attribute "route_number"
     */
    public function testPropertyRouteNumber()
    {
    }

    /**
     * Test attribute "date_of_travel"
     */
    public function testPropertyDateOfTravel()
    {
    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }
}
