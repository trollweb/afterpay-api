<?php
/**
 * CommonApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AfterPay
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Trollweb\AfterPayApi;

use \Trollweb\AfterPayApi\Configuration;
use \Trollweb\AfterPayApi\ApiClient;
use \Trollweb\AfterPayApi\ApiException;
use \Trollweb\AfterPayApi\ObjectSerializer;

/**
 * CommonApiTest Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\AfterPayApi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CommonApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for commonCustomerLookup
     *
     * Returns the customers address based on social security number or mobile number..
     *
     */
    public function testCommonCustomerLookup()
    {
    }

    /**
     * Test case for commonGetStatus
     *
     * Gets the status of the service.
     *
     */
    public function testCommonGetStatus()
    {
    }

    /**
     * Test case for commonGetVersion
     *
     * Gets the version of the service.
     *
     */
    public function testCommonGetVersion()
    {
    }

    /**
     * Test case for commonValidateAddress
     *
     * Check of the delivered customer addresses as well as a phonetic and associative identification of duplicates.  Additionally, checks of client specific negative or positive lists can be processed. Usually, the AddressCheck is  used for the pure verification of the address data e.g. for registration processes..
     *
     */
    public function testCommonValidateAddress()
    {
    }

    /**
     * Test case for commonValidateBankAccount
     *
     * Validates and evaluates the account and bank details in the context of direct debit payment.  It is possible to transfer either the combination of BankCode and AccountNumber or IBAN and BIC.
     *
     */
    public function testCommonValidateBankAccount()
    {
    }
}
